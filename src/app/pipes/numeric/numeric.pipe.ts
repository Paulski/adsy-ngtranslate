import {Pipe, PipeTransform} from '@angular/core';

/**
 * {{ item.your_float_value | numeric:{type: 'decimal', value: numeric_value_of_decimal} }}
 */
@Pipe({
	name: 'numeric'
})
export class NumericPipe implements PipeTransform
{

	/**
	 * Main action
	 * @param value
	 * @param args
	 * @returns {*}
	 */
	transform(value:any, args?:any):any
	{
		return this.processArgs(value, args);
	}

	/**
	 * Value processor
	 * @param value
	 * @param args
	 * @returns {any}
	 */
	private processArgs(value, args: any)
	{
		switch (true)
		{
			case (args['type'].indexOf('decimal') != -1 && typeof args['value'] === 'number'):
				value = value.toFixed(args['value']);
			break;
		}
		return value;
	}

}
