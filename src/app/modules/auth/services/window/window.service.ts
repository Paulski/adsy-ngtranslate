import {Output, EventEmitter, Injectable, Component, ViewChild} from '@angular/core';
import {ModalDirective} from 'ng2-bootstrap/ng2-bootstrap';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';

let template = require('./templates/modal-demo.html');

@Injectable()
export class WindowService
{

	@ViewChild('childModal')
	public childModal:ModalDirective;

	public subject:EventEmitter<string> = new EventEmitter<string>();

	constructor()
	{
	}

	public showChildModal():void
	{
		this.childModal.show();
	}

	public hideChildModal():void
	{
		this.childModal.hide();
	}

	createWindow(url:string, name:string = 'Window', width:number = 500, height:number = 600, left:number = 0, top:number = 0)
	{
		if (url == null) {
			return null;
		}

		this.subject.next('555');
		var options = `width=${width},height=${height},left=${left},top=${top}`;
	}

}