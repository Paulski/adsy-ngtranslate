import {Injectable, EventEmitter, Output} from '@angular/core';
import {AuthService} from "../auth/auth.service";
import {IAuthWatcher} from "../../model/IAuthWatcher.model";
import {IUserModel} from "../../model/user.model";
import {RestService} from "../../../http/rest.service";
import {AlertService} from "../../../notifications/alert/alert.service";

@Injectable()
export class UsersService
{
	private _auth:AuthService;

	constructor(private auth:AuthService, private rest: RestService, private alert: AlertService)
	{
		this._auth = auth;
		this._auth.subscribe((data:IAuthWatcher) => this.authenticate(data));
	}

	private authenticate(data:any)
	{
	}

	public get_user():IUserModel
	{
		return this._auth.getUserInfo();
	}

	public updateUser(params)
	{
		let self = this;
		this.rest.userModule.accountSettings(params, function(data)
		{
			if(data['status'] == 'true')
			{
				self.alert.alert({
					title: 'great',
					type: 'success',
					text: 'the_data_was_saved!'
				});
				self._auth.loadUser();
			}
		});
	}

	public changePassword(params)
	{
		let self = this;
		let requestBody = {
			current_password: params['current_password'],
			new_password: params['new_password'],
		};
		this.rest.userModule.changePassword(requestBody, function(data)
		{
			switch (data['status'])
			{
				case 'true':
					self.alert.alert({
						title: 'great',
						type: 'success',
						text: 'the_data_was_saved!'
					});
					break;
				case 'false':
					self.alert.alert({
						title: 'Oops',
						type: 'warning',
						text: 'the_data_wasnt_saved!'
					});
					break;
			}
		});
	}
	
	public getUserAdditional()
	{
		
	}

}
