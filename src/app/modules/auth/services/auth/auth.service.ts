import {Injectable, EventEmitter} from "@angular/core";
import {WindowService} from "../window/window.service";
import {Http, Headers, RequestOptions, Response, Request, RequestMethod, ResponseContentType} from "@angular/http";
import {Router} from "@angular/router";
import {env} from '../../../../../environments/environment';
import {Observable}     from 'rxjs/Observable';
import {RestService} from "../../../http/rest.service";

// Operators
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/toPromise';
import {LocalStorageService} from "ng2-webstorage/index";
import {IUserModel} from "../../model/user.model";
import {AlertService} from "../../../notifications/alert/alert.service";

export interface ISignInForm
{
	email:string;
	password:string;
	rememberMe:number;
}

export interface IAuthToken
{
	expires_in:number;
	access_token:string;
}

@Injectable()
export class AuthService
{
	private oAuthCallbackUrl:string;
	private oAuthTokenUrl:string;
	private oAuthUserUrl:string;
	private oAuthUserNameField:string;
	private authenticated:boolean = false;
	public static token:string = "";
	private expires:any = 0;
	private userInfo:IUserModel;
	private windowHandle:any = null;
	private intervalId:any = null;
	private expiresTimerId:any = null;
	private loopCount = 600;
	private intervalLength = 6000;
	private parsed:IAuthToken;
	private windowHandleLocation:string;

	private locationWatcher = new EventEmitter();
	private tokenReceived = new EventEmitter();
	private authenticatedWatcher = new EventEmitter();
	private errorRegister = new EventEmitter();
	private userReturned = new EventEmitter();
	private authURL:string = env.API_AUTH_URL;
	public waitAuth: boolean = true;

	constructor(private http:Http, private route:Router, private REST:RestService, private storage:LocalStorageService, private alert:AlertService)
	{
		this.checkTokenExist();
		this.subscribeByToken(data => this.authenticateByToken(data));
	}

	public doLogin(credentials:ISignInForm)
	{

		let self = this;
		this.authenticateByCredentials(credentials, function (data)
		{
			console.log(data);
			try {
				if(typeof data.access_token != 'undefined')
				{
					self.setToken(data.access_token);
				} else
				{
					self.alert.alert({
						title: 'Oops...',
						text: 'Wrong credentials, please try again.',
						type: 'error'
					});
					self.waitAuth = true;
					self.authenticated = false;
					self.emitAuthenticated();
				}
			} catch (e) {

			}
		});

	}

	public doLogout()
	{
		this.authenticated = false;
		this.expiresTimerId = null;
		this.expires = 0;
		AuthService.token = null;
		this.storage.clear('tokens');
		this.emitAuthStatus(true);
		console.log('Session has been cleared');
	}

	private emitAuthStatus(success:boolean)
	{
		this.emitAuthStatusError(success, null);
	}

	private emitAuthStatusError(success:boolean, error:any)
	{
		this.locationWatcher.emit(
			{
				success: success,
				authenticated: this.authenticated,
				token: AuthService.token,
				expires: this.expires,
				error: error
			}
		);
	}

	private emitUserReturned()
	{
		this.userReturned.emit();
	}

	private emitToken()
	{
		this.tokenReceived.emit();
	}

	private emitAuthenticated()
	{
		this.authenticatedWatcher.emit();
	}

	private emitErrorRegister()
	{
		this.errorRegister.emit();
	}

	public getSession()
	{
		return {authenticated: this.authenticated, token: AuthService.token, expires: this.expires};
	}

	private authenticateByCredentials(cresentials:ISignInForm, callback?:any)
	{
		this.REST.userModule.getAuthorization({
			'login': cresentials.email,
			'password': cresentials.password,
			'rememberMe': cresentials.rememberMe ? 1 : 0
		}, function (data)
		{
			if (typeof callback != 'undefined') {
				callback(data);
			}
		})
	}

	private extractData(res:Response)
	{
		let body = res.json();
		return body.data || {};
	}

	private authenticateByToken(data)
	{
		this.loadUser();
	}
	
	public loadUser()
	{
		let self = this;
		this.REST.userModule.getUser({}, function (data)
		{
			let user = data.json();
			if(typeof user != 'undefined')
			{
				self.setUserInfo(user);
				self.authenticated = true;
				self.expires = 56466465465;
				self.emitAuthenticated();
				self.emitAuthStatus(true);
			} else if (data == false)
			{
				self.waitAuth = true;
				self.authenticated = false;
				self.emitAuthenticated();
			}
		});
	}

	private fetchUserInfo()
	{
		if (AuthService.token != null) {
			var headers = new Headers();
			headers.append('Authorization', `Bearer ${AuthService.token}`);
			//noinspection TypeScriptUnresolvedFunction
			this.http.get(this.oAuthUserUrl, {headers: headers})
				.map(res => res.json())
				.subscribe(info =>
				{
					this.userInfo = info;
				}, err =>
				{
					console.error("Failed to fetch user info:", err);
				});
		}
	}

	public getUserInfo()
	{
		return this.userInfo;
	}

	public userExist(): boolean
	{
		return (typeof this.userInfo['first_name'] != 'undefined') ? true : false;

	}

	public getUserName()
	{
		return this.userInfo ? this.userInfo[this.oAuthUserNameField] : null;
	}

	private startExpiresTimer(seconds:number)
	{
		if (this.expiresTimerId != null) {
			clearTimeout(this.expiresTimerId);
		}
		this.expiresTimerId = setTimeout(() =>
		{
			console.log('Session has expired');
			this.doLogout();
		}, seconds * 1000); // seconds * 1000
		console.log('Token expiration timer set for', seconds, "seconds");
	}

	public subscribe(onNext:(value:any) => void, onThrow?:(exception:any) => void, onReturn?:() => void)
	{
		return this.locationWatcher.subscribe(onNext, onThrow, onReturn);
	}

	public subscribeByToken(onNext:(value:any) => void, onThrow?:(exception:any) => void, onReturn?:() => void)
	{
		return this.tokenReceived.subscribe(onNext, onThrow, onReturn);
	}

	public subscribeByAuthenticated(onNext:(value:any) => void, onThrow?:(exception:any) => void, onReturn?:() => void)
	{
		return this.authenticatedWatcher.subscribe(onNext, onThrow, onReturn);
	}

	public subscribeByErrorRegister(onNext:(value:any) => void, onThrow?:(exception:any) => void, onReturn?:() => void)
	{
		return this.errorRegister.subscribe(onNext, onThrow, onReturn);
	}

	public subscribeByUserReturned(onNext:(value:any) => void, onThrow?:(exception:any) => void, onReturn?:() => void)
	{
		return this.userReturned.subscribe(onNext, onThrow, onReturn);
	}

	public isAuthenticated()
	{
		return this.authenticated;
	}

	private parse(str)
	{ // lifted from https://github.com/sindresorhus/query-string
		if (typeof str !== 'string') {
			return {};
		}

		str = str.trim().replace(/^(\?|#|&)/, '');

		if (!str) {
			return {};
		}

		return str.split('&').reduce(function (ret, param)
		{
			var parts = param.replace(/\+/g, ' ').split('=');
			// Firefox (pre 40) decodes `%3D` to `=`
			// https://github.com/sindresorhus/query-string/pull/37
			var key = parts.shift();
			var val = parts.length > 0 ? parts.join('=') : undefined;

			key = decodeURIComponent(key);

			// missing `=` should be `null`:
			// http://w3.org/TR/2012/WD-url-20120524/#collect-url-parameters
			val = val === undefined ? null : decodeURIComponent(val);

			if (!ret.hasOwnProperty(key)) {
				ret[key] = val;
			} else
				if (Array.isArray(ret[key])) {
					ret[key].push(val);
				} else {
					ret[key] = [ret[key], val];
				}

			return ret;
		}, {});
	};

	public doAuth(credentials:ISignInForm)
	{
		this.doLogin(credentials);
	}

	public doRegister(credentials)
	{
		this.register(credentials);
	}

	private register(credentials)
	{
		let self = this;
		this.REST.userModule.register({
			'username': credentials.reg_email,
			'email': credentials.reg_email,
			'password': credentials.reg_password,
		}, function (data)
		{
			if(typeof data == 'string')
			{
				self.setToken(data);
			} else {
				self.emitErrorRegister();
			}
			console.log(data);
		})
	}
	
	public recoveryPassword(params: Object, callback: any)
	{
		this.REST.userModule.resetPassword(params, function (data)
		{
			callback(data);
		});
	}
	
	public recoveryPasswordConfirm(params: Object, postUrl, callback: any)
	{
		this.REST.userModule.resetPasswordConfirm(params, function (data)
		{
			callback(data);
		}, postUrl);
	}
	
	public static getToken(): string
	{
		return AuthService.token;
	}

	private setToken(token): void
	{
		AuthService.token = token;
		this.storage.store('token', token);
		this.emitToken();
	}

	public checkTokenExist()
	{
		let token = this.storage.retrieve('token');
		this.waitAuth = false;
		if(token != null)
		{
			this.waitAuth = false;
			this.setToken(token);
			this.authenticateByToken(token);
			return true;
		}

		return false;
	}

	private setUserInfo(user: any): void
	{
		this.userInfo = user;
		this.emitUserReturned();
	}
	

}
