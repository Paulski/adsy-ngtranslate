export interface IAuthWatcher
{
	success:boolean;
	authenticated:boolean;
	token:string;
	expires:number;
	error:any;
}