export interface IUserModel
{
	id:string
	country:string
	email:string
	phone:string
	first_name:string
	last_name:string
	skype_id:string
	phone_code:number
}