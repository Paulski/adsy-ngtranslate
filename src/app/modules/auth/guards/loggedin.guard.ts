import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {AuthService} from '../services/auth/auth.service';
import {IAuthWatcher} from "../model/IAuthWatcher.model";
import {Location} from '@angular/common';

@Injectable()
export class LoggedInGuard implements CanActivate
{

	private _auth:AuthService;
	private _router:Router;
	public _navRoute:any = "/";
	private authCredentials: IAuthWatcher;
	private _authValidation = 0;
	private _authCredentialsTMP:IAuthWatcher;

	constructor(auth:AuthService, router:Router, lc:Location)
	{
		this._auth = auth;
		this._router = router;
		this._navRoute = lc.path();
		this._auth.subscribe((data:IAuthWatcher) => this.setRollback(data, 'auth'));
		this._auth.subscribeByUserReturned((data:any) => this.setRollback(data, 'user'));
	}

	canActivate()
	{
		return this._auth.isAuthenticated();
	}

	private setRollback(data, type)
	{

		switch (true)
		{
			case type == 'auth':
				this._authCredentialsTMP = data;
				this._authValidation++;
				break;
			case type == 'user':
				this._authValidation++;
				break;
		}

		if(this._authValidation >= 2 && typeof this._authCredentialsTMP != 'undefined')
		{
			this.rollback();
		}

	}

	private rollback()
	{
		if(this._auth.userExist())
		{
			this.authCredentials = this._authCredentialsTMP;
			this.redirectToPage();
		} else
		{
			this._auth.subscribeByUserReturned(data => this.redirectToPage());
		}
	}

	private redirectToPage()
	{
		if (this.authCredentials.authenticated == true && this.authCredentials.success == true && this._auth.isAuthenticated() == true) {
			this._router.navigate([this._navRoute]);
		}
	}

	public redirectByRoute(link: string)
	{
		console.log(link);
		console.log(this._navRoute);
		this._router.navigate([link]);
	}
}