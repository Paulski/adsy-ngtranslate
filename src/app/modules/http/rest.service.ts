import {Injectable} from '@angular/core';
import {HttpService, HttpHelper} from './http.service';
import {env} from "../../../environments/environment";

@Injectable()
export class RestService
{

	private static httpService:HttpService;

	constructor(private http:HttpService)
	{
		RestService.httpService = http;
	}

	public fbModule =
	{
		getFBLinks: function (params:Object = {}, callback:any)
		{
			RestService.httpService.get(env.API_BASE_URL + '/fb/defaults').setToken().setParameters(params).than(res =>
			{
				callback(res);
			});
		}
	};

	public filters =
	{
		getFBFilters: function (callback:any)
		{
			RestService.httpService.get(env.API_BASE_URL + '/fb/filter-params').setToken().than(res =>
			{
				callback(res);
			});
		},
		getBalanceFilters: function (callback:any)
		{
			RestService.httpService.get(env.API_BASE_URL + '/balance/filter-params').setToken().than(res =>
			{
				callback(res);
			});
		}
	};

	public userModule =
	{
		getAuthorization: function (params:Object, callback:any)
		{
			RestService.httpService.post(env.API_AUTH_URL).setParameters(params).than(res =>
			{
				callback(res);
			})
		},
		getUser: function (params:Object, callback:any)
		{
			RestService.httpService.get(env.API_BASE_URL + '/user/user').setToken().than(res =>
			{
				callback(res);
			})
		},
		register: function (params:Object, callback:any)
		{
			RestService.httpService.post(env.API_REGISTER_URL).setParameters(params).than(res =>
			{
				callback(res);
			})
		},
		accountSettings: function (params:Object, callback:any)
		{
			RestService.httpService.put(env.API_BASE_URL + '/user-params/update').setToken().setParameters(params).than(res =>
			{
				callback(res);
			})
		},
		changePassword: function (params:Object, callback:any)
		{
			RestService.httpService.put(env.API_BASE_URL + '/user/settings/change-password').setToken().setParameters(params).than(res =>
			{
				callback(res);
			})
		},
		resetPassword: function (params:Object, callback:any)
		{
			RestService.httpService.post(env.API_BASE_URL + '/user/recovery/request').setParameters(params).than(res =>
			{
				callback(res);
			})
		},
		resetPasswordConfirm: function (params:Object, callback:any, postUrlParams)
		{
			RestService.httpService.post(env.API_BASE_URL + '/user/recovery/reset' + HttpHelper.ObjToGet(postUrlParams)).setParameters(params).than(res =>
			{
				callback(res);
			})
		}
	};

	// REST Balance
	public balance =
	{
		getBalance: function (params:Object, callback:any)
		{
			RestService.httpService.get(env.API_BASE_URL + '/balance').setToken().setParameters(params).than(res =>
			{
				callback(res);
			})
		}
	};

	public cart =
	{
		addOffers: function (params:Object, callback:any)
		{
			RestService.httpService.post(env.API_BASE_URL + '/cart/create').setToken().setParameters(params).than(res =>
			{
				callback(res);
			})
		},
		convertOffers: function (params:Object, callback:any)
		{
			RestService.httpService.put(env.API_BASE_URL + '/cart/convert').setToken().setParameters(params).than(res =>
			{
				callback(res);
			})
		},
		getOffers: function (params:Object, callback:any)
		{
			RestService.httpService.get(env.API_BASE_URL + '/cart').setToken().setParameters(params).than(res =>
			{
				callback(res);
			})
		},
		getTotalSum: function (params:Object, callback:any)
		{
			RestService.httpService.get(env.API_BASE_URL + '/cart/total-sum').setToken().setParameters(params).than(res =>
			{
				callback(res);
			})
		},
		changeLink: function (params:Object, callback:any)
		{
			RestService.httpService.put(env.API_BASE_URL + '/cart/change-anchor').setToken().setParameters(params).than(res =>
			{
				callback(res);
			})
		}
	};

	public project =
	{
		addProject: function (params:Object, callback:any)
		{
			RestService.httpService.post(env.API_BASE_URL + '/project').setToken().setParameters(params).than(res =>
			{
				callback(res);
			})
		}
	};

	public addFunds =
	{
		getAddFunds: function (params:Object, callback:any)
		{
			RestService.httpService.get(env.API_BASE_URL + '/balance/add-funds').setToken().setParameters(params).than(res =>
			{
				callback(res);
			})
		},
		requestAddFunds: function (params:Object, callback:any, url:string)
		{
			RestService.httpService.post(url).setToken().setParameters(params).than(res =>
			{
				callback(res);
			})
		}
	}

	public keyword =
	{
        addKeyword: function(params:Object, callback:any) {
            RestService.httpService.post(env.API_BASE_URL + '/keyword').setToken().setParameters(params).than(res =>
            {
                callback(res);
            })
        }
	};
}
