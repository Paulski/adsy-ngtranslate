import {Injectable, Inject} from '@angular/core';
import {Http, Headers, Request, Response, RequestOptions} from '@angular/http';
import {AuthService} from "../auth/services/auth/auth.service";
import {Observable} from "rxjs/Observable";
import {Observer} from "rxjs/Observer";
import "rxjs/add/observable/of";
import "rxjs/add/operator/share";
import "rxjs/add/operator/map";
import "rxjs/add/operator/merge";
import {AlertService} from "../notifications/alert/alert.service";

@Injectable()
export class HttpService
{
	private _http:Http;
	private _query:Query;
	private _token:string;

	/**
	 *
	 * @param http
	 */
	constructor(http:Http, private alert:AlertService)
	{
//		this.auth = auth;
		this.set_http(http);
	}

	/**
	 *
	 * @param url
	 * @returns {HttpService}
	 */
	public get(url:string):HttpService
	{
		this.set_query(new Query());
		this._query.setType('get');
		this._query.setUrl(url);
		return this;
	}

	/**
	 *
	 * @param url
	 * @returns {HttpService}
	 */
	public put(url:string):HttpService
	{
		this.set_query(new Query());
		this._query.setType('put');
		this._query.setUrl(url);
		return this;
	}

	/**
	 *
	 * @param url
	 * @returns {HttpService}
	 */
	public post(url:string):HttpService
	{
		this.set_query(new Query());
		this._query.setType('post');
		this._query.setUrl(url);
		return this;
	}

	/**
	 *
	 * @param parameters
	 * @returns {HttpService}
	 */
	public setParameters(parameters:any):HttpService
	{
		this._query.setParameters(parameters);
		return this;
	}

	/**
	 *
	 * @returns {Promise<R>|Promise<ErrorObservable>|Promise<ErrorObservable|T>|Observable<R>|webdriver.promise.Promise<R>|any}
	 */
	private callPost():any
	{
		let query = this._query;
		let parameters = query.parametersExist() == true ? query.getParameters() : {};
		let body = HttpHelper.ObjToPost(parameters);
		let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});

		headers.append('Accept', 'application/json');
		if(this.tokenExist())
		{
			headers.append('Authorization', 'Bearer '+this.getToken());
		}

		let options = new RequestOptions({headers: headers});

		return this._http.post(query.getUrl(), body, options)
			.map(this.extractData)
			.catch(this.handleError);
	}

	/**
	 *
	 * @param res
	 * @returns {{}}
	 */
	private extractData(res:Response)
	{
		var body = {};

		switch (true) {
			case (typeof res != 'Object'):
				body = res.json();
				break;
			case (typeof res == 'Object'):
				body = res;
				break;
		}

		return body || {};
	}

	/**
	 *
	 * @param data
	 */
	private subscribe(data)
	{
		let query = this._query;
		if (typeof query.getCallback() != 'undefined') {
			let callback = query.getCallback();
			callback(data);
		}
	}

	/**
	 *
	 * @returns {undefined|Subscription|AnonymousSubscription|*|TeardownLogic}
	 */
	private callGet():any
	{
		let query = this._query;
		let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
		let parameters = query.parametersExist() == true ? query.getParameters() : {};
		let url = query.getUrl() + HttpHelper.ObjToGet(parameters);

		headers.append('Accept', 'application/json');

		if(this.tokenExist())
		{
			headers.append('Authorization', 'Bearer '+this.getToken());
		}

		return this._http.get(url, {headers: headers})
			.share()
			.subscribe(data =>
			{
				if (typeof query.getCallback() != 'undefined') {
					let callback = query.getCallback();
//					this.destroy_query();
					callback(data);
				}
			});
	}

	/**
	 *
	 * @param error
	 * @returns {ErrorObservable}
	 */
	private handleError(error:any)
	{
//		console.log(error);
		let errMsg = (error.message) ? error.message :
			error.status ? `${error.status} - ${error.statusText}` : 'Server error';

		this.alert.alert({
			title: 'Oops...',
			text: 'Something went wrong!',
			type: 'error'
		});
		return Observable.throw(errMsg);
	}

	/**
	 *
	 * @param callback
	 */
	public than(callback:any)
	{
		switch (true) {
			case (this._query.getType() == 'get'):
				this.callGet();
				break;
			case (this._query.getType() == 'post'):
				this.callPost().subscribe(
					data => this.subscribe(data),
					error => this.handleError(error));
				break;
			case (this._query.getType() == 'put'):
				this.callPut().subscribe(
					data => this.subscribe(data),
					error => this.handleError(error));
				break;
		}

		this._query.setCallback(callback);
	}

	/**
	 *
	 * @returns {Http}
	 */
	private get_http():Http
	{
		return this._http;
	}

	/**
	 *
	 * @param value
	 */
	private set_http(value:Http)
	{
		this._http = value;
	}

	/**
	 *
	 * @returns {Query}
	 */
	private get_query():Query
	{
		return this._query;
	}

	/**
	 *
	 * @param value
	 */
	private set_query(value:Query)
	{
		this._query = value;
	}

	private destroy_query()
	{
		this._query = undefined;
	}

	callPut():any
	{
		let query = this._query;
		let parameters = query.parametersExist() == true ? query.getParameters() : {};
		let body = HttpHelper.ObjToPost(parameters);
		let headers = new Headers({
			'Content-Type': 'application/x-www-form-urlencoded'
		});
		headers.append('Accept', 'application/json');
		if(this.tokenExist())
		{
			headers.append('Authorization', 'Bearer '+this.getToken());
		}

		let options = new RequestOptions({headers: headers});

		return this._http.put(query.getUrl(), body, options)
			.map(this.extractData)
			.catch(this.handleError);
	}

	callDelete():any
	{
		let id = 0;
		return this._http.delete(`url with id`) // ...using put request
			.map((res:Response) => res.json()) // ...and calling .json() on the response to return data
			.subscribe(
				comments =>
				{
					// Emit list event
				},
				err =>
				{
					// Log errors if any
				}
			);
	}

	public setToken()
	{
		this._token = AuthService.getToken();
		return this;
	}

	public getToken(): string
	{
		return this._token;
	}

	public tokenExist(): boolean
	{
		return typeof this._token != 'undefined' ? true : false;
	}
}

export class Query
{
	private _url:string;
	private _parameters:any;
	private _callback:any;
	private _type:string;

	public getUrl():string
	{
		return this._url;
	}

	public setUrl(value:string)
	{
		this._url = value;
	}

	public getParameters():any
	{
		return this._parameters;
	}

	public setParameters(value:any)
	{
		this._parameters = value;
	}

	public getCallback():any
	{
		return this._callback;
	}

	public setCallback(value:any)
	{
		this._callback = value;
	}

	getType():string
	{
		return this._type;
	}

	setType(value:string)
	{
		this._type = value;
	}

	public parametersExist()
	{
		return typeof this.getParameters() == 'undefined' ? false : true;
	}
}

export class HttpHelper
{

	public static ObjToGet(object:Object):string
	{
		let serialize = function(obj, prefix) {
			var str = [], p;
			for(p in obj) {
				if (obj.hasOwnProperty(p)) {
					var k = prefix ? prefix + "[" + p + "]" : p, v = obj[p];
					str.push((v !== null && typeof v === "object") ?
						serialize(v, k) :
					encodeURIComponent(k) + "=" + encodeURIComponent(v));
				}
			}
			return str.join("&");
		};

		let str = serialize(object, '');

		return str != '' ? "?" + str : "";
	}

	public static ObjToPost(object:Object):string
	{
		let serialize = function(obj, prefix) {
			var str = [], p;
			for(p in obj) {
				if (obj.hasOwnProperty(p)) {
					var k = prefix ? prefix + "[" + p + "]" : p, v = obj[p];
					str.push((v !== null && typeof v === "object") ?
						serialize(v, k) :
					encodeURIComponent(k) + "=" + encodeURIComponent(v));
				}
			}
			return str.join("&");
		};

		let str = serialize(object, '');

		return str != '' ? "" + str : "";
	}

}
