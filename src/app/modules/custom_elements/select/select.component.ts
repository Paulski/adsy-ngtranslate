import {Component, OnInit, Input, ElementRef} from '@angular/core';

@Component({
    selector: 'filter-select',
    templateUrl: './select.component.html',
    styleUrls: ['./select.component.css'],
    host: {
        '(document:click)': 'onClick($event)',
    }
})

export class SelectComponent implements OnInit {
    @Input()
    private id: string;
    @Input()
    private name: string;
    @Input()
    private placeholder: string;
    @Input()
    private items: Array<any>;

    private is_open: boolean = false;
    private current_item: any = null;

    constructor(private _eref: ElementRef) {
    }

    ngOnInit() {
    }

    private toggle_dropdown(): void {
        this.is_open = this.is_open ? false : true;
    }

    private click_item(e: Event, item): void {
        this.placeholder = item.text;
        this.current_item = item;

        this.toggle_dropdown();
    }

    onClick(event): void {
        if (!this._eref.nativeElement.contains(event.target)) {
            this.is_open = false;
        }
    }
}
