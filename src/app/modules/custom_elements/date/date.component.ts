import {Component, OnInit, Input, ElementRef} from '@angular/core';

import * as moment from 'moment';

@Component({
    selector: 'filter-date',
    templateUrl: './date.component.html',
    styleUrls: ['./date.component.css'],
    host: {
        '(document:click)': 'onClick($event)',
    },
    outputs: [
        "ngModelChange"
    ]
})
export class DateComponent implements OnInit {
    @Input()
    private id: string;
    @Input()
    private placeholder: string;

    private is_open: boolean = false;

    public dt_from: Date = new Date();
    public dt_to: Date = new Date();

    public model_from = '';
    public model_to = '';
    public mask_from = [/\d/, /\d/, /\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/,];
    public mask_to = [/\d/, /\d/, /\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/,];

    private customClasses: Array<any>;

    constructor(private _eref: ElementRef) {
        this.dt_from = new Date(moment().subtract(1, 'months').format('YYYY/MM/DD'));
        this.dt_to = new Date(moment().format('YYYY/MM/DD'));

        this.customClasses = this.getDatesRange(this.dt_from, this.dt_to);
    }

    ngOnInit() {
    }

    private toggle_dropdown(): void {
        this.is_open = this.is_open ? false : true;
    }

    private onClick(event): void {
        if (!this._eref.nativeElement.contains(event.target)) {
            //this.is_open = false;
        }
    }

    private getDatesRange(startDate, stopDate) {
        let dateArray = [];
        let currentDate = moment(startDate);
        while (currentDate <= stopDate) {
            dateArray.push(
                {date: moment(currentDate).toDate().setHours(0, 0, 0, 0), mode: 'day', clazz: 'btn-active-datepicker'}
            );
            currentDate = moment(currentDate).add(1, 'days');
        }
        return dateArray;
    }

    private onChange(event): void {
        this.customClasses = this.getDatesRange(this.dt_from, this.dt_to);
    }

    private applyFilter(event): void {
        this.placeholder = moment(this.dt_from).format('YYYY/MM/DD') + " - " + moment(this.dt_to).format('YYYY/MM/DD');
        this.is_open = false;
    }

    private clearFilter(event): void {
        this.placeholder = "Chose your date";
        this.is_open = false;
    }

}
