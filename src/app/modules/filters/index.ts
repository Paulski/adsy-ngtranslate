import {NgModule, ModuleWithProviders} from "@angular/core";
import {FiltersService} from "./services/filters.service";
import {FiltersComponent} from "./components/filters.component";
import {InputsComponent} from "./components/inputs/inputs.component";

export * from "./services/filters.service";
export * from "./components/filters.component";
export * from "./components/inputs/inputs.component";

// for angular-cli
export default {
	directives: [FiltersComponent, InputsComponent],
	providers: [FiltersService]
};

@NgModule({
	providers: [
		FiltersService
	]
})
export class FiltersModule
{
	public static forRoot():ModuleWithProviders
	{
		return {
			ngModule: FiltersModule,
			providers: [FiltersService]
		};
	}
}