import {Component, OnInit, Input, EventEmitter} from '@angular/core';
import {FiltersService} from "../../services/filters.service";

declare var _:any;

@Component({
	selector: 'app-inputs',
	templateUrl: './inputs.component.html',
	styleUrls: ['./inputs.component.css']
})
export class InputsComponent implements OnInit
{

	@Input()
	private name:string;
	@Input()
	private key:string;
	@Input()
	private type:string;
	@Input()
	public form:string;
	@Input()
	public datasource:string;
	@Input()
	public min:number;
	@Input()
	public max:number;
	@Input()
	public selected:string;
	@Input()
	private placeholder:string;
	@Input()
	private disabled:boolean;
	@Input()
	private eventEmitter:any;

	private value:Array<any> = [];
	private valueSelector:Array<any> = [];
	private event = new EventEmitter();
	private is_open:boolean = false;
	private current_item:any = null;

	constructor(private filtersService:FiltersService)
	{
	}

	/**
	 * Setter of attribute - "value"
	 * @param value
	 */
	private setValue(value:any):void
	{
		this.value = value;
	}

	ngOnInit()
	{
	}

	ngAfterViewInit()
	{
		switch (true) {
			case ((this.type == 'select_dropdown' || this.type == 'select_dropdown_group' || this.type == 'select_dropdown_single')
			&& this.hasDatasource()):
				this.processStorageFiltersDropdown();
				break;
		}
		this.checkSelected(true);
	}

	private processStorageFilters(recursive?:boolean)
	{
		let self = this;
		let storageData = this.filtersService.getFilterFromStorage(this.datasource);

		switch (true) {
			case storageData != null:

				switch (true) {
					case typeof storageData['key'] != 'undefined':
						this.setKey(storageData['key']);
						break;
				}
				this.valueSelector = this.filtersService.makeRange(storageData['min'], storageData['max'], this.selected, this.datasource);
				break;
			default:
				if (typeof recursive == 'undefined') {
					this.filtersService.getFiltersFromApi(this.form, function (status)
					{
						if (status == true) {
							self.processStorageFilters(true);
						}
					});
				}
				break;
		}
	}

	private setKey(key:string):void
	{
		this.key = key;
	}

	private processStorageFiltersDropdown(recursive?:boolean)
	{
		let self = this;
		let storageData = this.filtersService.getFilterFromStorage(this.datasource);
		switch (true) {
			case storageData != null:
				this.valueSelector = this.filtersService.makeRange(this.min, this.max, this.selected, this.datasource);
				break;
			default:
				if (typeof recursive == 'undefined') {
					this.filtersService.getFiltersFromApi(this.form, function (status)
					{
						if (status == true) {
							self.processStorageFiltersDropdown(true);
						}
					});
				}
				break;
		}
	}

	private click_item(e:Event, item):void
	{
		item['selected'] = item['selected'] ? false : true;
		if (this.type == 'select_dropdown_single') {
			this.checkSelected(true, item);
		} else {
			this.checkSelected(true);
		}

		let form = this.form;
		this.event.emit(
			{
				status: 'changed',
				form: form
			}
		);

		this.current_item = item;
		this.toggle_dropdown();
		this.emitChanges();
	}

	private checkSelected(clearPlaceholder?: boolean, item?: any)
	{
		let self = this;
		let result = [];
		if(typeof clearPlaceholder != 'undefined')
		{
			this.placeholder = '';
		}
		let hasItem = typeof item != 'undefined';
		_.forEach(this.valueSelector, function (value)
		{
			if (self.has(value, 'selected')) {
				if(hasItem && value != item)
				{
					value['selected'] = false;
				}
				if(value['selected'] == true)
				{
					self.placeholder += (self.placeholder.length > 0 ? ', ': '') + value['name'];
					result.push(value);
				}
			}
			if (self.has(value, 'value') && self.isArray(value['value'])) {
				_.forEach(value['value'], function (row)
				{
					if (self.has(row, 'selected')) {
						if(hasItem && row['name'] != item['name'])
						{
							row['selected'] = false;
						}
						if(row['selected'] == true)
						{
							self.placeholder += (self.placeholder.length > 0 ? ', ': '') + row['name'];
							result.push(row);
						}
					}
				});
			}
		});
		this.setValue(result);
	}

	private has(data, key)
	{
		return _.has(data, key) ? true : false;
	}

	private emitChanges()
	{
		if (typeof this.eventEmitter != 'undefined') {
			this.eventEmitter.emit(this.value);
		}
	}

	private hasDatasource():boolean
	{
		return typeof this.datasource != 'undefined' ? true : false;
	}

	private isArray(value:any):boolean
	{
		return value instanceof Array;
	}


	private toggle_dropdown():void
	{
		this.is_open = this.is_open ? false : true;
	}

}
