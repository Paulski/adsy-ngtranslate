import {Component, OnInit, Input, EventEmitter, ViewChildren, ElementRef, HostListener} from '@angular/core';
import {FiltersService} from "../services/filters.service";
import {LocalStorageService} from "ng2-webstorage/index";

declare var _:any;
declare var $:any;

import * as moment from 'moment';

@Component({
	selector: 'filter',
	templateUrl: './filters.component.html',
	styleUrls: ['./filters.component.css']
})
export class FiltersComponent implements OnInit
{
	/**
	 * Name like "Page - Rank"
	 */
	@Input()
	private name:string;
	@Input()
	private key:string;
	@Input()
	private type:string;
	@Input()
	public form:string;
	@Input()
	public datasource:string;
	@Input()
	public min:number;
	@Input()
	public max:number;
	@Input()
	private selected:string;
	@Input()
	private placeholder:string;
	@Input()
	private disabled:boolean;
	@Input()
	private relationDisabled:string;
	@Input()
	private relationSource:string;
	@Input()
	private is_search:boolean = false;
	@Input()
	private multiple:boolean = false;

	public value:any;
	private valueSelector:Array<any> = [];
	private event = new EventEmitter();
	private is_open:boolean = false;
	public current_item:any = null;
	public relation_ids:Array<number> = [];
	private waitForResponse:Object = {};
	private _storageData:any;

	/*
	 Additional variables for filter
	 */
	private maxDropdownMenuHeight:number = 0;
	private previousItem:any;
	private basePlaceholder:string = '';
	private currentUnixTime: number =this.getRandomInt(256,1024);

	/* Date variable */
	public transfer_time_from:Date = new Date();
	public transfer_time_to:Date = new Date();
	public mask_date = [/\d/, /\d/, /\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/];
	private customClasses:Array<any>;

	/**
	 * Listen filter elements of current component
	 */
	@ViewChildren('filter')
	private childQuery:any;
	@ViewChildren('dropDownMenu')
	private dropDownMenu:any;

	constructor(private filtersService:FiltersService, private storage:LocalStorageService)
	{
		this.transfer_time_from = new Date(moment().subtract(1, 'months').format('YYYY/MM/DD'));
		this.transfer_time_to = new Date(moment().format('YYYY/MM/DD'));

		this.customClasses = this.getDatesRange(this.transfer_time_from, this.transfer_time_to);
	}

	get selected_val():string
	{
		let selected = null;

		if( this.has(this.selected, 'length') && this.selected.length > 0 || parseInt(this.selected) > 0)
		{
			selected = this.selected;
		} else if (this.has(this.storageData, 'selected') && this.storageData['selected'].length > 0 || parseInt(this.storageData['selected']) > 0 && selected == null)
		{
			selected = this.storageData['selected'];
		}

		return selected;
	}

	set selected_val(value:string)
	{
		this.selected = value;
	}

	get storageData():any
	{
		return this._storageData;
	}

	set storageData(value:any)
	{
		this._storageData = value;
	}

	get placeholder_val():string
	{
		let result = '';
		if (this.basePlaceholder.length > 0) {
			result = this.placeholder;
			result = result.replace(this.basePlaceholder, '');
		}
		if (this.basePlaceholder.length == 0) {
			this.basePlaceholder = this.placeholder;
			result = '';
			this.placeholder = '';

		}
		if (result.length == 0) {
			result = this.basePlaceholder;
		}
		return result;
	}

	set placeholder_val(value:string)
	{
		this.placeholder = value;
	}

	/**
	 * This function register the new filter in service
	 */
	ngOnInit()
	{
		this.filtersService.registerNewFilter(this);
	}

	ngOnChanges()
	{
		this.reloadDatasource();
		this.checkSelected();
	}

	/**
	 * This function get current value of field ang pass it in value object
	 */
	ngAfterViewInit()
	{
		this.filtersService.subscribeFilter(this.form + this.datasource, data => this.reloadDatasource());
		switch (true) {
			case (this.type == 'select' && this.hasDatasource()):
			case (this.type == 'select_group' && this.hasDatasource()):
				this.processStorageFilters();
				try {
					let val = this.childQuery.first.nativeElement.value;
					if (typeof val != 'undefined') {
						this.setValue(val);
					}
				} catch (e) {

				}
				break;
			case (this.type == 'range-slider-single' && this.hasDatasource()):
			case (this.type == 'range-slider-multiple' && this.hasDatasource()):
				this.processStorageFilters();
				this.specialFilterLoader();
				break;
			case ((this.type == 'select_dropdown' || this.type == 'select_dropdown_group' || this.type == 'select_dropdown_single')
			&& this.hasDatasource()):
				this.processStorageFiltersDropdown();
				break;
			case (this.type == 'datepicker' && this.hasDatasource()):
				this.processStorageFiltersDate();
				break;
		}
		this.checkSelected();
	}

//	@HostListener('window:scroll', ['$event'])
//	processScroll(event)
//	{
////		if(['select_dropdown_group'].indexOf(this.type) != -1)
////		{
////			this.calcMaxDropdownMenuHeight();
////		}
//	}

	private calcMaxDropdownMenuHeight()
	{
//		let el = this.dropDownMenu[0].nativeElement;
//		console.log(el);
	}

	private setStorageKey(key:string)
	{
		this.setKey(key);
	}

	private getOffsetTop(elem)
	{
		var offsetLeft = 0;
		let i = 0;
		do {
			if (!isNaN(elem.offsetTop)) {
				offsetLeft += elem.offsetTop;
			}
			i++;
		} while (elem = elem['offsetParent']);
		return offsetLeft;
	}

	public reloadDatasource()
	{
		this.storageData = this.filtersService.getFilterFromStorage(this.datasource);
		if (this.storageData != null) {
			this.valueSelector = this.filtersService.makeRange(this.storageData['min'], this.storageData['max'], this.selected_val, this.datasource);
		}
	}

	private getValueDataSource()
	{
		this.storageData = this.filtersService.getFilterFromStorage(this.datasource);
		if (this.storageData != null) {
			return this.filtersService.makeRange(this.storageData['min'], this.storageData['max'], this.selected_val, this.datasource);
		}
		return false;
	}

	private processStorageFilters(recursive?:boolean)
	{
		let self = this;
		this.storageData = this.filtersService.getFilterFromStorage(this.datasource);

		switch (true) {
			case this.storageData != null:

				switch (true) {
					case typeof this.storageData['key'] != 'undefined':
						this.setStorageKey(this.storageData['key']);
						break;
				}
				this.valueSelector = this.filtersService.makeRange(this.storageData['min'], this.storageData['max'], this.selected_val, this.datasource);
				break;
			default:
				if (typeof recursive == 'undefined') {
					this.loadFilters(function (data)
					{
						switch (data) {
							case true:
								self.processStorageFilters(true);
								break;
							case false:
								setTimeout(function ()
								{
									self.processStorageFilters(true);
								}, 1000)
								break;
						}
					});
				}
				break;
		}
	}

	/**
	 * Function for loading plugin for special type of filters
	 * @param type
	 */
	private specialFilterLoader()
	{
		let self = this;
		switch (this.type)
		{
			case 'range-slider-multiple':
				if(this.storageData != null)
				{
					$("#ex"+this.currentUnixTime).slider({
						step: 1,
						min: this.storageData['min'],
						max: this.storageData['max'],
						value: [this.storageData['min'], this.storageData['max']]
					}).on('slideStop', function (data)
					{
						self.setValue(data.value);
						self.event.emit(
							{
								status: 'changed',
								form: self.form
							}
						);
					});
				}
				break;
		}
	}

	private getRandomInt(min, max)
	{
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	private processStorageFiltersDropdown(recursive?:boolean)
	{
		let self = this;
		this.storageData = this.filtersService.getFilterFromStorage(this.datasource);
		switch (true) {
			case this.storageData != null:
				this.setStorageKey(this.storageData['key']);
				this.valueSelector = this.filtersService.makeRange(this.min, this.max, this.selected_val, this.datasource);
				break;
			default:
				if (typeof recursive == 'undefined') {
					this.loadFilters(function (data)
					{
						switch (data) {
							case true:
								self.processStorageFiltersDropdown(true);
								break;
							case false:
								setTimeout(function ()
								{
									self.processStorageFiltersDropdown(true);
								}, 1000)
								break;
						}
					});
				}
				break;
		}
	}

	private processStorageFiltersDate(recursive?:boolean)
	{
		let self = this;
		this.storageData = this.filtersService.getFilterFromStorage(this.datasource);
		switch (true) {
			case this.storageData != null:
				if (typeof this.storageData != 'undefined') {
					this.setStorageKey(this.storageData['key']);
					if (this.has(this.storageData, 'value') && this.storageData['value'].length == 2) {
						if (this.has(this.storageData['value'][0], 'transfer_time_from')) {
							this.transfer_time_from = moment.unix(parseInt(this.storageData['value'][0]['transfer_time_from'])).toDate();
						}
						if (this.has(this.storageData['value'][1], 'transfer_time_to')) {
							this.transfer_time_to = moment.unix(parseInt(this.storageData['value'][1]['transfer_time_to'])).toDate();
						}
					}
				}
				break;
			default:
				if (typeof recursive == 'undefined') {
					this.loadFilters(function (data)
					{
						switch (data) {
							case true:
								self.processStorageFiltersDate(true);
								break;
							case false:
								setTimeout(function ()
								{
									self.processStorageFiltersDate(true);
								}, 1000)
								break;
						}
					});
				}
				break;
		}
	}

	private loadFilters(callback:any)
	{
		let checkFilterLoad = this.filtersService.inFilterLoadingStack(this.form);
		this.filtersService.filterLoaderStack[this.form] = true;
		console.log('нужны новые фильтра');
		switch (checkFilterLoad) {
			case false:
				this.filtersService.getFiltersFromApi(this.form, function (status)
				{
					callback(status);
				});
				break;
			case true:
				callback(false);
				break;
		}
	}

	private hasDatasource():boolean
	{
		return typeof this.datasource != 'undefined' ? true : false;
	}

	/**
	 * Function for receive event of filter
	 * @param event
	 */
	private onChange(event):void
	{
		this.setValue(event.target.value);
		let form = this.form;
		this.event.emit(
			{
				status: 'changed',
				form: form
			}
		);
	}

	private onChangeDate(event):void
	{
		this.customClasses = this.getDatesRange(this.transfer_time_from, this.transfer_time_to);
	}

	/**
	 * Function for subscribe on filter events
	 * @param onNext
	 * @param onThrow
	 * @param onReturn
	 * @returns {any}
	 */
	public subscribe(onNext:(value:any) => void, onThrow?:(exception:any) => void, onReturn?:() => void)
	{
		return this.event.subscribe(onNext, onThrow, onReturn);
	}

	/**
	 * Setter of attribute - "value"
	 * @param value
	 */
	private setValue(value:any):void
	{
		switch (this.multiple) {
			case true:
				if (typeof this.value == 'undefined') {
					this.value = [];
				}
				this.value.push(value);
				break;
			case false:
				this.value = value;
				break;
		}
	}

	private unsetValue(value)
	{
		this.value = this.getValue().filter(function (el)
		{
			return el != value;
		});
	}

	/**
	 * Getter of attribute - "value"
	 * @returns {any}
	 */
	public getValue():any
	{
		return this.value;
	}

	private setKey(key:string):void
	{
		this.key = key;
	}

	public getKey():string
	{
		return typeof this.valueSelector['key'] != 'undefined' ? this.valueSelector['key'] : this.key;
	}

	private toggleDropdown():void
	{
		this.is_open = this.is_open ? false : true;
	}

	private clickItem(item):void
	{
		let value = '';
		let arrayResult = false;
		let selected = false;

		if (this.has(item, 'selected') && item['selected'] == true) {
			selected = true;
		}

		if (this.multiple) {
			arrayResult = true;
		}

		if (this.type == 'select_dropdown_single') {
			if (arrayResult) {
				switch (selected) {
					case true:
						this.placeholder_val = this.placeholder_val.replace(item.name + ',', '');
						break;
					case false:
						this.placeholder_val = this.placeholder_val + item.name + ',';
						break;
				}
			} else {
				this.placeholder_val = item.name;
			}
			value = item.value;
		} else {
			if (item.value instanceof Array) {
				this.placeholder_val = item.name;
				value = item.value;
			} else {
				if (arrayResult) {
					switch (selected) {
						case true:
							this.placeholder_val = this.placeholder_val.replace(item.value + ',', '');
//							console.log(arrayResult, selected);
							break;
						case false:
							this.placeholder_val = this.placeholder_val + item.value + ',';
							break;
					}
				} else {
					this.placeholder_val = item.value;
				}
				value = item.name;
			}
		}


		this.current_item = item;
		this.setOpen(false);


		switch (this.multiple) {
			case true:
				switch (selected) {
					case true:
						this.unsetValue(value);
						break;
					case false:
						this.setValue(value);
						break;
				}
				break;
			case false:
				this.setValue(value);
				break;
		}

		let form = this.form;
		this.event.emit(
			{
				status: 'changed',
				form: form
			}
		);

		//required in down side of function
		this.toggleSelectedHighlights(item);
	}

	private toggleSelectedHighlights(item)
	{
		if (!this.multiple) {
			if (this.has(this.previousItem, 'selected')) {
				this.previousItem['selected'] = false;
			}
			this.previousItem = item;
		}
		item['selected'] = item['selected'] ? false : true;
	}

	public checkSelected()
	{
		let self = this;
		_.forEach(this.valueSelector, function (item)
		{
			if (self.has(item, 'value') && self.isArray(item.value)) {
				_.forEach(item.value, function (subItem)
				{
					if (self.has(subItem, 'selected') && subItem['selected'] === true) {
						self.clickItem(subItem);
					}
//					console.log(subItem);
				});
			}
		});
//		if(value === true)
//		{
//			console.log(55);
//		}
	}

	private has(data:any, key:string)
	{
		return _.has(data, key) ? true : false;
	}

	private isArray(value:any):boolean
	{
		return value instanceof Array;
	}

	private setEnabled():void
	{
		if (typeof this.relationSource != 'undefined') {
			this.relation_ids = [];
			let relation = this.filtersService.proccessRelation(this.relationSource, this.form);
			if (this.has(relation.current_item, 'name')) {
				if (typeof relation.current_item.value == 'object') {
					let tmp:Array<number> = [];
					_.forEach(relation.current_item.value, function (row)
					{
						tmp.push(row['name']);
					});
					this.relation_ids = tmp;
				} else {
					this.relation_ids.push(relation.current_item['name']);
				}
			}
		}
		this.disabled = false;
	}

	public getRelationDisabled()
	{
		return this.relationDisabled;
	}

	public setOpen(open:boolean):void
	{
		this.is_open = open;
	}

	private checkRelation(item:any):boolean
	{
		return _.includes(this.relation_ids, item['name']);
	}

	private getDatesRange(startDate, stopDate)
	{
		let dateArray = [];
		let currentDate = moment(startDate);
		while (currentDate <= stopDate) {
			dateArray.push(
				{date: moment(currentDate).toDate().setHours(0, 0, 0, 0), mode: 'day', clazz: 'btn-active-datepicker'}
			);
			currentDate = moment(currentDate).add(1, 'days');
		}
		return dateArray;
	}

	private applyFilter(event):void
	{
		this.placeholder_val = moment(this.transfer_time_from).format('YYYY/MM/DD') + " - " + moment(this.transfer_time_to).format('YYYY/MM/DD');
		this.is_open = false;

		this.setValue(
			{
				transfer_time_from: moment(this.transfer_time_from).unix(),
				transfer_time_to: moment(this.transfer_time_to).unix()
			}
		);
		let form = this.form;
		this.event.emit(
			{
				status: 'changed',
				form: form
			}
		);
	}

	private clearFilter(event):void
	{
		this.placeholder_val = "Chose your date";
		this.is_open = false;
	}

	private searchFilter(text:string):void
	{
		let self = this;
		if (text.length > 0) {
			this.valueSelector = _.filter(this.getValueDataSource(), function (obj)
			{
				if (obj['name'].indexOf(text) > -1) {
					return true;
				}
				obj['value'] = _.filter(obj['value'], function (obj_sub)
				{
					if (obj_sub['value'].indexOf(text) > -1) {
						return true;
					}
				});
				if (obj['value'].length > 0) {
					return true;
				}
			});
		} else {
			this.reloadDatasource();
		}
	}

}
