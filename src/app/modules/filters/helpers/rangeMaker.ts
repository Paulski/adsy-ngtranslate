import {LocalStorageService, IStorage} from "ng2-webstorage/index";

declare var _: any;

export class rangeMaker
{
	private _min: number;
	private _max: number;
	private _selected: any;
	private _storage: IStorage;
	private _storageKey: string;
	private _options: Array<Object> = [];

	constructor(min:number, max:number, selected: any, storage:IStorage, storageKey:string = '')
	{
		this.setMin(min);
		this.setMax(max);
		this.setSelected(selected)
		this.setStorage(storage);
		this.setStorageKey(storageKey);
		this.process();
	}

	private getMin():number
	{
		return this._min;
	}

	private setMin(value:number)
	{
		this._min = value;
	}

	private getMax():number
	{
		return this._max > 0 ? this._max + 1 : this._max;
	}

	private setMax(value:number)
	{
		this._max = value;
	}

	private getStorage():IStorage
	{
		return this._storage;
	}

	private setStorage(value:IStorage)
	{
		this._storage = value;
	}

	private getStorageKey():string
	{
		return this._storageKey;
	}

	private setStorageKey(value:string)
	{
		this._storageKey = value;
	}

	private getSelected():any
	{
		return typeof this._selected == 'undefined' ? 0 : this._selected;
	}

	private setSelected(value:any)
	{
		this._selected = value;
	}

	private addToOptions(name:string, value: any, selected: boolean, key: string)
	{
		this._options.push({
			name: name,
			value: value,
			selected: selected,
            key: key
		})
	}

	public getOptions()
	{
		return this._options;
	}

	private process()
	{
		switch (true)
		{
			case this.isStorageData():
				this.processStorage();
			break;
			default:
				this.processBase();
			break;
		}
	}

	private getFromStorageByKey()
	{
		return this.getStorage().retrieve(this.getStorageKey());
	}

	private processBase()
	{
		let self = this;

		_.forEach(_.range(this.getMin(), this.getMax()), function(val, index)
		{
			let selected = _.isEqual(self.getSelected(),index) ? true : false;
			self.addToOptions(index, index, selected, '');
		});
	}

	private processStorage()
	{
		let storageData = this.getFromStorageByKey();
		let self = this;
		let valSelected = this.getSelected();
		switch (true)
		{
			case typeof storageData != 'undefined' && !this.has(storageData, 'value'):
				this.processBase();
			break;
            case storageData != null && typeof storageData.value != "undefined":
				_.forEach(storageData['value'], function(val, index) {
					switch (true) {
						case typeof val == 'object':
							let sub_value: Array<Object> = [];
							_.forEach(val, function (col, row)
							{
								let selected = false;
								if(typeof valSelected != 'undefined' && valSelected != null)
								{
									selected = _.isEqual(valSelected.toLowerCase(), row.toLowerCase()) ? true : false;
								}
                                sub_value.push({
                                    name: row,
                                    value: col,
                                    selected: selected,
                                });
							});
							self.addToOptions(index, sub_value, false, storageData['key']);
						break;
						default:
							let selected = _.isEqual(self.getSelected(),index) ? true : false;
							self.addToOptions(val, index, selected, storageData['key']);
						break;
					}
				});
			break;
		}
	}

	private has(data, key)
	{
		return _.has(data, key) ? true : false;
	}

	private isStorageData(): boolean
	{
		return this.getStorageKey().length > 0 ? true : false;
	}
}