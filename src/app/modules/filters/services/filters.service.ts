import {Injectable, EventEmitter} from '@angular/core';
import {rangeMaker} from "../helpers/rangeMaker";
import {LocalStorageService, IStorage} from "ng2-webstorage/index";
import {RestService} from "../../http/rest.service";
import {AuthService} from "../../auth/services/auth/auth.service";

declare var _:any;

@Injectable()
export class FiltersService
{

	private forms:any = [];
	private formEmitter = new EventEmitter();
	private filterLoaded:Object = [];
	private filterPrefix:string = 'filter_';
	public filterLoaderStack:Object = {};

	constructor(private storage:LocalStorageService, private rest:RestService, private auth:AuthService)
	{
//		let rm = new rangeMaker(0, 10, 'en', storage, 'countries');
		if (this.auth.checkTokenExist()) {
			this.loadFilters({});
		}
		this.auth.subscribeByToken(data => this.loadFilters(data));
	}

	private loadFilters(data)
	{
//		this.getFiltersFromApi('balance');
//		this.getFiltersFromApi('fb');
	}

	public subscribeFilter(form, onNext:(value:any) => void, onThrow?:(exception:any) => void, onReturn?:() => void)
	{
		if (typeof this.formEmitter[form] == 'undefined') {
			let emiter = new EventEmitter();
			this.filterLoaded[form] = emiter;
		}
		return this.filterLoaded[form].subscribe(onNext, onThrow, onReturn);
	}

	/**
	 * Function register the new filter of form
	 * @param filter
	 */
	public registerNewFilter(filter)
	{
		let currentFilter = filter;
		currentFilter.subscribe(value => this.eventHandler(value));
		if (typeof this.forms[currentFilter.form] == 'undefined') {
			this.forms[currentFilter.form] = [];
			this.emitFormAdd(currentFilter.form);
		}
		this.forms[currentFilter.form].push(currentFilter);
	}

	public getFiltersFromApi(key:string, callback?:any, copydata?:Object)
	{
		let self = this;

		switch (key) {
			case 'fb':
				this.rest.filters.getFBFilters(function (data)
				{
					if (typeof data == 'object') {
						_.forEach(data.json(), function (value, index)
						{
							self.storage.store(self.filterPrefix + key + "_" + index, value);
						});
					}

					self.emitFilterLoad(key);
					if (typeof callback != 'undefined') {
						callback(true);
					}
				});
				break;
			case 'addtocart':
			case 'change_in_cart':
			case 'change_in_cart_global':
			case 'cart':
			case 'balance':
				this.rest.filters.getBalanceFilters(function (data)
				{
					if (typeof data == 'object') {
						_.forEach(data.json(), function (value, index)
						{
							if (typeof copydata != 'undefined' && typeof copydata[key] != 'undefined') {
								_.forEach(copydata[key]['fields'], function (subValue, subIndex)
								{
									if (subValue == index) {
										self.storage.store(self.filterPrefix + copydata[key]['replaceKey'] + "_" + subValue, value);
									}
								});
							}
							self.storage.store(self.filterPrefix + key + "_" + index, value);
						});
					}
					self.emitFilterLoad(key);
					if (typeof callback != 'undefined') {
						callback(true);
					}
				});
				break;
		}
	}

	public getFilterFromStorage(key:string):any
	{
		return this.storage.retrieve(this.filterPrefix + key);
	}

	/**
	 * Function emit event of form added
	 * @param formName
	 */
	private emitFormAdd(formName)
	{
		this.formEmitter.emit(
			{
				status: 'add',
				form: formName
			}
		);
	}

	public emitFilterLoad(form:string)
	{
		if (this.has(this.filterLoaded, form)) {
			this.filterLoaded[form].emit();
		}
	}

	private has(data:any, key:string)
	{
		return _.has(data, key) ? true : false;
	}

	public makeRange(min:number = 0, max:number = 10, selected:any, storageKey:string = ''):any
	{
		return new rangeMaker(min, max, selected, this.storage, this.filterPrefix + storageKey).getOptions();
	}

	private getFiltersCredentials(form:string)
	{

	}

	/**
	 * Function emit event of filter changed
	 * @param formName
	 */
	private emitFormFiltersChange(formName):void
	{
		this.formEmitter.emit(
			{
				status: 'changedFilter',
				form: formName,
				filters: this.processFiltersToObject(formName)
			}
		);
	}

	/**
	 * Function for subscribe on form emitter
	 * @param onNext
	 * @param onThrow
	 * @param onReturn
	 * @returns {any}
	 */
	public subscribe(onNext:(value:any) => void, onThrow?:(exception:any) => void, onReturn?:() => void)
	{
		return this.formEmitter.subscribe(onNext, onThrow, onReturn);
	}

	/**
	 * Function return list of form filters
	 * @param form
	 * @returns {{}}
	 */
	public getFilters(form:string):any
	{
		return typeof this.forms[form] != 'undefined' ? this.forms[form] : [];
	}

	/**
	 * Function process event of filters
	 * @param data
	 */
	private eventHandler(data)
	{
		this.emitFormFiltersChange(data.form);
	}

	/**
	 * Function processing filters to object like {name: value} of filter input field
	 * @param formName
	 * @returns {{}}
	 */
	public processFiltersToObject(formName)
	{
		let self = this;
		let result = {};
		this.getFilters(formName).forEach(function (entry)
		{
			let relationDisabled = entry.getRelationDisabled();
			
			//process relation between selects
			if (typeof relationDisabled != 'undefined') {
				_.forEach(self.forms[formName], function (row)
				{
					if (row.getKey() == relationDisabled) {
						row.setEnabled();
					}
				});
			}
			if (entry.getValue() != '' && typeof entry.getValue() != "undefined") {
				result[entry.getKey()] = entry.getValue();
			}

		});
		return result;
	}

	public proccessRelation(key:string, formName:string)
	{
		let el = null;
		_.forEach(this.forms[formName], function (row)
		{
			if (row.getKey() == key) {
				el = row;
			}
		});
		return el;
	}

	public inFilterLoadingStack(key:string)
	{
		return typeof this.filterLoaderStack[key] != 'undefined';
	}

}
