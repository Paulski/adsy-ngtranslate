declare var _:any;

export class AddFundsParser
{

	private responseKeys =
	{
		main: 'first',
		other: ['second', 'third']
	};

	private paymentTypes = {
		main: [],
		other: []
	};

	private _error:boolean = false;

	private formInputTypes:Array<string> = ['input-text', 'select', 'radio'];

	private _response:Object = {};

	get error():boolean
	{
		return this._error;
	}

	set error(value:boolean)
	{
		if (this._error == false && value == true) {
			this._error = value;
		}
	}

	get response():Object
	{
		return this._response;
	}

	set response(value:Object)
	{
		this._response = value;
	}

	public processAddFundsResponse(response:Object)
	{
		this.response = response;
		this.fillTypes();
		return this;
	}

	private fillTypes()
	{
		let self = this;
		_.forEach(this.response, function (value, i)
		{
			let key = self.searchKeyByValues(i, self.responseKeys);
			switch (true) {
				case (key != null && key.length > 0):
					self.setTypes(key, value);
					break;
			}
		})
	}

	private setTypes(key, value):void
	{
		switch (key) {
			case 'main':
				this.paymentTypes[key] = value;
				break;
			case 'other':
				this.paymentTypes[key].push(value[0]);
				break;
		}
	}

	private searchKeyByValues(data, arrayOfKeys):string
	{
		let result = null;
		_.forEach(arrayOfKeys, function (value, i)
		{
			if (value.indexOf(data) != -1) {
				result = i;
			}
		});
		return result;
	}

	public getPaymentTypes()
	{
		return this.paymentTypes;
	}

	private has(data:any, key:string):boolean
	{
		return _.has(data, key) ? true : false;
	}

	public makeForm(form:Object)
	{
		let resultForm = {};
		let self = this;
		_.forEach(form, function (value)
		{
			_.forEach(value, function (input)
			{
				switch (input['type']) {
					case 'radio':
					case 'input-text':
						switch (true) {
							case self.has(input, 'required') && input['required'] == true && (input['value'].length > 0 || input['value'] == true || parseInt(input['value']) > 0):
								resultForm[input['name']] = input['value'];
								break;
							case (self.has(input, 'required') && input['required'] == false) || !self.has(input, 'required'):
								resultForm[input['name']] = input['value'];
								break;
							case self.has(input, 'required') && input['required'] == true && (input['value'].length == 0 || input['value'] == false):
								self.error = true;
								break;
						}
						break;
					case 'select':

						switch (true) {
							case self.has(input, 'required') && input['required'] == true && input['selected'].length > 0:
								resultForm[input['name']] = input['selected'];
								break;
							case (self.has(input, 'required') && input['required'] == false) || !self.has(input, 'required'):
								resultForm[input['name']] = input['value'];
								break;
							default:
								self.error = true;
								break;
						}
						break;
				}
			});
		});
		return !this.error ? resultForm : {error: true};
	}

}