import {Injectable} from '@angular/core';
import {RestService} from "../../http/rest.service";
import {AddFundsParser} from "../classes/addFundsParser";
import {AlertService} from "../../notifications/alert/alert.service";

declare var _:any;

@Injectable()
export class AddFundsService
{

	private parser:AddFundsParser;
	private _paymentTypes = {
		main: [],
		other: []
	};

	private submitForm:Object = {};

	get paymentTypes()
	{
		return this._paymentTypes;
	}

	set paymentTypes(value)
	{
		this._paymentTypes = value;
	}

	constructor(private rest:RestService, private alert:AlertService)
	{
		let self = this;
		this.parser = new AddFundsParser();
		this.rest.addFunds.getAddFunds({}, function (data)
		{
			self.paymentTypes = self.parser.processAddFundsResponse(data.json()).getPaymentTypes();
		});
	}

	public getPaymentTypes()
	{
		return this.paymentTypes;
	}

	public processPaymentSubmit(paymentObject:Object):boolean
	{
		let form = this.parser.makeForm(paymentObject['form']['step']);
		let result = true;
		switch (true) {
			case this.has(form, 'error') && form['error'] == true:
				result = false;
				break;
			case !this.has(form, 'error'):
				result = true;
				if (this.has(paymentObject['form'], 'url') && this.has(paymentObject['form'], 'submit_type')) {
					switch (paymentObject['form']['submit_type']) {
						case 'submit':
							this.rest.addFunds.requestAddFunds(form, data =>  this.processResponse(data), paymentObject['form']['url']);
							break;
						case 'redirect':
							let getQuery = this.ObjToGet(form);
							window.location.href = paymentObject['form']['url'] + getQuery;
							break;
					}
				}
				break;
		}
		return result;
	}

	private processResponse(data)
	{
		switch (true) {
			case this.has(data, 'status') && data['status'] == true && this.has(data, 'url'):
				window.location.href = data['url'];
				break;
			default:
			case this.has(data, 'status') && data['status'] == false:
				this.alert.alert({
					type: 'warning',
					title: 'Oops',
					text: data['error_msg']
				});
				break;

		}
	}

	private has(data:any, key:string):boolean
	{
		return _.has(data, key) ? true : false;
	}

	private ObjToGet(object:Object):string
	{
		let serialize = function (obj, prefix)
		{
			var str = [], p;
			for (p in obj) {
				if (obj.hasOwnProperty(p)) {
					var k = prefix ? prefix + "[" + p + "]" : p, v = obj[p];
					str.push((v !== null && typeof v === "object") ?
						serialize(v, k) :
					encodeURIComponent(k) + "=" + encodeURIComponent(v));
				}
			}
			return str.join("&");
		};

		let str = serialize(object, '');

		return str != '' ? "?" + str : "";
	}


}
