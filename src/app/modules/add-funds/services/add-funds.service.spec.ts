/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AddFundsService } from './add-funds.service';

describe('Service: AddFunds', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AddFundsService]
    });
  });

  it('should ...', inject([AddFundsService], (service: AddFundsService) => {
    expect(service).toBeTruthy();
  }));
});
