export interface Pagination
{
	total: number;
	current: number;
	perPage: number;
	last: Object;
	next: Object;
	self: Object;
}