import {Component, OnInit, EventEmitter, Input, Output, Renderer, Self, ElementRef} from '@angular/core';
import {ControlValueAccessor, NgModel} from '@angular/forms';

export interface SortChangedEvent
{
	sort:string;
	column:string;
}

@Component({
	selector: 'datatable-sort',
	templateUrl: './sort.component.html',
	styleUrls: ['./sort.component.css'],
	providers: [NgModel]
})
export class SortComponent implements OnInit, ControlValueAccessor
{
	@Input()
	private column:string;

	@Output()
	public sortChanged:EventEmitter<SortChangedEvent> = new EventEmitter<SortChangedEvent>(false);

	private _cd:NgModel;
	public renderer:Renderer;
	public elementRef:ElementRef;

	get cd():NgModel
	{
		return this._cd;
	}

	set cd(value:NgModel)
	{
		this._cd = value;
	}

	private icons:Object = Object.create({
		asc: 'arrow_downward',
		desc: 'arrow_upward',
		none: 'compare_arrows'
	});

	private _sortKey:string = 'none';

	get sortKey():string
	{
		return this._sortKey;
	}

	set sortKey(value:string)
	{
		this._sortKey = value;
		this.cd.viewToModelUpdate(value);
		this.sortChanged.emit({
			sort: value,
			column: this.column
		});
	}

	constructor(@Self() cd:NgModel, renderer:Renderer, elementRef:ElementRef)
	{
		this.cd = cd;
		this.renderer = renderer;
		this.elementRef = elementRef;
		cd.valueAccessor = this;
	}

	ngOnInit()
	{
	}

	private getIcon()
	{
		return typeof this.cd.value != 'undefined' ? this.icons[this.cd.value] : 'none';
	}

	public writeValue(value:number):void
	{
	}

	public registerOnChange(fn:(_:any) => {}):void
	{
	}

	public registerOnTouched(fn:() => {}):void
	{
	}

	private toggleSort():void
	{
		switch (this.cd.viewModel) {
			case 'none':
				this.sortKey = 'asc';
				break;
			case 'asc':
				this.sortKey = 'desc';
				break;
			case 'desc':
				this.sortKey = 'none';
				break;
			case null:
				this.sortKey = 'none';
				break;
		}
	}

}
