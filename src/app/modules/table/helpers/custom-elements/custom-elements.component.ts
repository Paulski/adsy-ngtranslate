import {Component, OnInit, Input} from '@angular/core';
import {TableService} from "../../service/table.service";

declare var _:any;
declare var $:any;

@Component({
	selector: 'custom-elements',
	templateUrl: './custom-elements.component.html',
	styleUrls: ['./custom-elements.component.css']
})
export class CustomElementsComponent implements OnInit
{

	@Input()
	private source:any;
	@Input()
	private tableKey:any;
	@Input()
	private item:any;
	@Input()
	private event:any;
	@Input()
	private attached:any;
	@Input()
	private value:any;
	@Input()
	private preview:any;

	private icon:string = '';
	private hasPreview: boolean = false;

	constructor(private table:TableService)
	{
	}

	ngOnInit()
	{
		switch (true) {
			case this.source['type'] == 'icon' && this.has(this.source, 'imgLink'):
				this.icon = this.source['imgLink'];
				break;
		}
		this.checkPreview();
	}

	ngAfterViewInit()
	{
//		switch (true) {
//			case this.source['type'] == 'icon' && this.has(this.source, 'imgLink'):
//				this.icon = this.source['imgLink'];
//				break;
//		}
		this.checkPreview();

		$('[data-toggle="tooltip"]').tooltip({
			html: true,
			placement: "top",
			template: '<div class="tooltip" style="background-color: white" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
		});
	}
	
	private checkPreview()
	{
		this.hasPreview = this.has(this.preview, 'prefix');
	}

	private emit(param?:any)
	{
		if (this.defined(this.value) && this.value != null) {
//			this.value = this.value == 1 ? null : 1;
		}
		switch (true) {
			case this.source['type'] == 'radiogroup_two':
				this.table.emitTable(this.tableKey, this.event, {item: this.item, value: param});
				break;
			case this.source['type'] == 'event_with_preview':
				this.table.emitTable(this.tableKey, this.event, {item: this.item, value: param});
				break;
			default:
				this.table.emitTable(this.tableKey, this.event, this.item);
				break;
		}
	}

	private toggleImg()
	{
		//this.icon = this.has(this.source, 'imgToggle') && this.icon == this.source['imgLink'] ? this.source['imgToggle'] : this.source['imgLink'];
	}

	private checkInAttached():any
	{
		let result = null;
		switch (true) {
			case this.defined(this.value && this.value != null):
				result = this.value == 1 ? true : null;
				break;
			default:

				break;
		}
		if (typeof this.attached != 'undefined' && this.has(this.attached[0], 'data') && this.has(this.attached[0]['processEvents'], this.event)) {
			let self = this;
			result = _.find(this.attached[0]['data'], function (obj)
			{
				return obj == self.item;
			});
		}
		return result != null || (typeof result != 'undefined' && result != null) ? true : null;
	}

	private checkInAttachedImg()
	{
		let check = this.checkInAttached();
		switch (true) {
			case check == null:
				this.icon = this.source['imgLink'];
				break;
			case check == true:
				this.icon = this.source['imgToggle'];
				break;
		}
	}

	ngOnDestroy()
	{
		$('[data-toggle="tooltip"]').tooltip('destroy');
	}

	private defined(value):boolean
	{
		return typeof value != 'undefined';
	}

	private has(data:any, key:string)
	{
		return _.has(data, key) ? true : false;
	}

}
