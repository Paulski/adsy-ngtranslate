import {Directive, EventEmitter, TemplateRef, Input, Output, Renderer, ElementRef} from '@angular/core';
import {TooltipModule} from 'ng2-bootstrap/ng2-bootstrap';

@Directive({
	selector: '[tooltip]',
	exportAs: 'bs-tooltip'
})
export class TooltipDirective
{
	@Input('tooltip')
	public content:string;
	@Input('tooltipHtml')
	public htmlContent:string | TemplateRef<any>;
	@Input('tooltipPlacement')
	private placement:string = 'top';
	@Input('tooltipIsOpen')
	private isOpen:boolean;
	@Input('tooltipEnable')
	private enable:boolean;
	@Input('tooltipAppendToBody')
	private appendToBody:boolean;
	@Input('tooltipClass')
	public popupClass:string;
	@Input('tooltipContext')
	public tooltipContext:any;
	@Input('tooltipPopupDelay')
	public delay:number = 0;
	@Output()
	public tooltipStateChanged:EventEmitter<boolean>;

	constructor(renderer:Renderer, el:ElementRef)
	{
	}

	ngAfterViewInit()
	{
	}

}
