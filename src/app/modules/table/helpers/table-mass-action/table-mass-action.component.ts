import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

declare var $:any;

export interface MassChangedEvent
{
	event:string;
}

@Component({
	selector: 'table-mass-action',
	templateUrl: './table-mass-action.component.html',
	styleUrls: ['./table-mass-action.component.css']
})
export class TableMassActionComponent implements OnInit
{

	@Input()
	private list:Array<Object> = [];

	@Output()
	public massAction:EventEmitter<MassChangedEvent> = new EventEmitter<MassChangedEvent>(false);

	constructor()
	{
	}

	ngOnInit()
	{
	}

	ngAfterViewInit()
	{
	}

	private emitEvent(event: string)
	{
		this.massAction.emit(
			{
				event: event
			});
	}

}
