declare var _:any;

export class Compare
{

	private _current:any;


	get current():any
	{
		return this._current;
	}

	set current(value:any)
	{
		this._current = value;
	}

	constructor()
	{
	}

	public setCurrent(value)
	{
		if (typeof this.current == 'undefined') {
			this.current = value;
		}
	}

	public isEqualObjects(value)
	{
		let result:boolean = true;
		switch (true) {
			case this.defined(this.current) && this.defined(value):
				result = this.isEqual(this.current, value);
				this.current = value;
				break;
			case !this.defined(this.current):
				result = false;
				this.current = value;
				break;
		}
		return result;
	}

	public hasEqualKey(value, field): boolean
	{
		let self= this;
		let result = _.find(value, function (obj)
		{
			return self.has(obj, 'item') && self.has(obj.item, field);
		});

		return this.length(result) > 0;
	}

	private length(data)
	{
		return _.size(data);
	}

	private defined(value):boolean
	{
		return typeof value != 'undefined';
	}

	private isEqual(first, last):boolean
	{
		return _.isEqual(first, last);
	}

	private has(data:any, key:string):boolean
	{
		return _.has(data, key);
	}

}