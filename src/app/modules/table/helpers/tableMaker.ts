import {Compare} from "./compare";
declare var _:any;

export class tableMaker
{
	private datasource:Array<Object> = [];
	private availableListOfFields:Object = {};
	private _checkKeyChanges:Array<string> = [];
	private _comparator:Compare;
	private _sumByKey:string;
	private _groupByKey:string;
	private _selectedEvent:string;

	get comparator():Compare
	{
		return this._comparator;
	}

	set comparator(value:Compare)
	{
		this._comparator = value;
	}

	get checkKeyChanges():Array<string>
	{
		return this._checkKeyChanges;
	}

	set checkKeyChanges(value:Array<string>)
	{
		this._checkKeyChanges = value;
	}

	get sumByKey():string
	{
		return this._sumByKey;
	}

	set sumByKey(value:string)
	{
		this._sumByKey = value;
	}

	get groupByKey():string
	{
		return this._groupByKey;
	}

	set groupByKey(value:string)
	{
		this._groupByKey = value;
	}

	get selectedEvent():string
	{
		return this._selectedEvent;
	}

	set selectedEvent(value:string)
	{
		this._selectedEvent = value;
	}

	constructor(datasource:Array<Object> = [], availableListOfFields:Object = {}, checkKeyChanges?:Array<string>, groupByKey?:string, sumByKey?:string, selectedEvent?:string)
	{
		if (this.defined(checkKeyChanges)) {
			this.comparator = new Compare();
			this.checkKeyChanges = checkKeyChanges;
		}

		if (this.defined(groupByKey)) {
			this.groupByKey = groupByKey;
		}

		if (this.defined(sumByKey)) {
			this.sumByKey = sumByKey;
		}

		if (this.defined(selectedEvent) && this.defined(sumByKey)) {
			this.selectedEvent = selectedEvent;
		}

		this.setDataSource(datasource);
		this.setAvailableListOfFields(availableListOfFields);
		this.process();
	}

	private setDataSource(datasource)
	{
		this.datasource = datasource;
	}

	private setAvailableListOfFields(availableListOfFields)
	{
		this.availableListOfFields = availableListOfFields;
	}

	public getDataSource()
	{
		return this.datasource;
	}

	private getAvailableFieldsList()
	{
		return this.availableListOfFields;
	}

	private process()
	{
		let stack = this.getDataSource();
		let availableFieldsList = this.getAvailableFieldsList();
		let data = [];
		let specialHold = '';
		let self = this;
		let comparedObjects = [];
		let listItemsRef = {};

		if (typeof stack != 'undefined' && _.size(stack) > 0) {
			_.forEach(stack, function (value, index)
			{
				let item = [];
				_.forEach(availableFieldsList['fields'], function (col, row)
				{
					switch (true) {
						case typeof col['column'] != 'undefined':
							//Used factorial when it place in object
							let elem = value[col['column']];

							if (self.has(col, 'factor') && col['factor'] >= 1 && !self.has(col, 'factorColumn')) {
								elem *= col['factor'];
							}
							if (self.has(col, 'factorColumn')) {
								elem *= value[col['factorColumn']] == 0 ? 1 : 6;
							}

							//Used pipe when it place in object
							if (self.has(col, 'pipe')) {
								elem = col['pipe']['class'].transform(elem, col['pipe']['arguments']);
							}

							//check special preprocessor of view
							let special = self.has(col, 'special') ? col['special'] : '';
							if (specialHold.length == 0) {
								specialHold = special;
							}

							item.push(
								{
									'item': {
										value: elem,
										class: self.has(col, 'class') ? col['class'] : '',
										key: self.has(col, 'key') ? col['key'] : '',
										special: special
									}
								});
							break;
						case typeof col['custom'] != 'undefined':
							let innerItem = {
								valueCustom: self.has(col['custom'], 'value') ? value[col['custom'].value] : null,
								custom: col['custom'],
								valueColumn: value[col['custom'].valueColumn],
								event: col['custom'].event
							};
							if (self.has(col['custom'], 'preview')) {
								let preview = {};
								preview['prefix'] = value[col['custom']['preview']['prefix']];
								preview['end'] = value[col['custom']['preview']['end']];
								innerItem = Object.assign(innerItem, {preview: preview});
							}
							item.push(
								{
									'item': innerItem
								});
							break;
					}
				});

				//Group operation by key
				if (self.defined(self.groupByKey)) {
					let groupValue = '';
					let prevKey = '';
					for (let j = 0; j < item.length; j++) {
						if (item[j]['item']['key'] == self.groupByKey) {
							groupValue = item[j]['item']['value'];
						}
					}
					if (groupValue.length > 0) {
						switch (self.has(listItemsRef, groupValue)) {
							case true :
								listItemsRef[groupValue].push(item);
								break;
							case false :
								listItemsRef[groupValue] = [];
								listItemsRef[groupValue].push(item);
								break;
						}
					}
				}

				if (!self.defined(self.groupByKey)) {
					data.push(item);
				}

			});

			if (self.defined(self.groupByKey)) {
				for (let item in listItemsRef) {
					data = data.concat(listItemsRef[item]);
					listItemsRef[item] = [];
				}
			}

			if (data.length > 0) {
				//Check changes of values by key, between elements (row`s)
				if (self.defined(self.checkKeyChanges) && self.checkKeyChanges.length > 0) {
					let prevKey = '';
					for (let i = 0; i < data.length; i++) {
						for (let j = 0; j < data[i].length; j++) {
							if (this.checkKeyChanges.indexOf(data[i][j]['item']['key']) != -1 && data[i][j]['item']['value'] != prevKey) {
								prevKey = data[i][j]['item']['value'];
								data[i][0]['item']['customKey'] = specialHold;
								comparedObjects.push(data[i]);
							}
						}
					}
				}
			}
		}

		let headers:Array<Object> = [];
		_.forEach(availableFieldsList['fields'], function (col, row)
		{
			headers.push({
				title: col['title'],
				width: self.has(col, 'width') ? col['width'] : '',
				class: self.has(col, 'class') ? col['class'] : '',
				column: self.has(col, 'column') ? col['column'] : '',
				key: self.has(col, 'key') ? col['key'] : '',
				special: self.has(col, 'special') ? col['special'] : '',
				sort: self.has(col, 'sort') && col['sort'] == true ? true : false
			});
		});

		data['length'] = _.size(data);

		let selected:number = 0;
		let selectedPositions:Array<number> = [];

		if (typeof self.selectedEvent != 'undefined') {
			_.forEach(data, function (value, index)
			{
				let elem = _.find(value, function (elem)
				{
					return self.has(elem, 'item') && self.has(elem['item'], 'event') && elem.item['event'] == self.selectedEvent && elem.item['valueCustom'] > 0;
				});
				if (typeof elem != 'undefined') {
					selected++;
					selectedPositions.push(index);
				}
			});
		}

		let result:Object = {
			headers: headers,
			data: data,
			extended: {
				selected: selected,
				selectedPositions: selectedPositions,
				length: data.length,
				sum: 0
			}
		};

		if (self.defined(self.groupByKey) && self.defined(self.sumByKey) && comparedObjects.length > 0) {
			_.forEach(comparedObjects, function (el)
			{
				let count = 0;
				let sum = 0;
				let itemGroup = _.filter(el, function (obj)
				{
					return self.has(obj, 'item') && obj.item['key'] == self.groupByKey;
				});
				result['data'].filter(function (obj)
				{
					let elem = _.find(obj, function (elem)
					{
						return self.has(elem, 'item') && elem.item['key'] == self.groupByKey && elem.item['value'] == itemGroup[0]['item']['value'];
					});

					if (self.has(elem, 'item')) {
						let sumValue = _.find(obj, function (elem)
						{
							return self.has(elem, 'item') && elem.item['key'] == self.sumByKey;
						});
						if (self.has(sumValue, 'item')) {
							sum += parseFloat(sumValue['item']['value']);
						}
						count++;
					}
				});
				itemGroup[0]['item']['count'] = count;
				itemGroup[0]['item']['sum'] = sum;
			})
		}

		this.setDataSource((typeof result != 'undefined') ? result : {});
	}

	private compareObjects(value)
	{
		let self = this;
		let compareObj = {};
		_.forEach(this.checkKeyChanges, function (el)
		{
			compareObj[el] = _.find(value, function (obj)
			{
				return self.has(obj, 'item') && obj.item['key'] == el;
			});

		});
		return this.comparator.isEqualObjects(compareObj);
	}

	private defined(value):boolean
	{
		return typeof value != 'undefined';
	}

	private has(data:any, key:string)
	{
		return _.has(data, key) ? true : false;
	}
}