import {Injectable, EventEmitter} from '@angular/core';

declare var _ : any;

@Injectable()
export class TableService
{

	private tableEmitterList: Object = {};

	constructor()
	{
	}

	public registerTable(key:string)
	{
		switch (true)
		{
			case typeof key != 'undefined': 
				this.addTable(key);
				break;
		}
		return this;
	}
	
	private addTable(key)
	{
		if(!this.has(this.tableEmitterList, key))
		{
			this.tableEmitterList[key] = new EventEmitter();
		}
	}
	
	private has(data, key)
	{
		return _.has(data, key) ? true : false;
	}

	public emitTable(key: string, event: string, element: any)
	{
		switch (true)
		{
			case typeof key != 'undefined':
				this.tableEmitterList[key].emit(
					{
						event: event,
						element: element
					});
				break;
		}
	}
	
	public subscribe(key: any, onNext:(value:any) => void, onThrow?:(exception:any) => void, onReturn?:() => void)
	{
//		if(!this.has(this.tableEmitterList, key))
//		{
//			this.tableEmitterList[key] = new EventEmitter();
//		}
		return this.tableEmitterList[key].subscribe(onNext, onThrow, onReturn);
	}

	public unsubscribe(key)
	{
//		if(!this.has(this.tableEmitterList, key))
//		{
//			this.tableEmitterList[key] = new EventEmitter();
//		}
		this.tableEmitterList[key].unsubscribe();
//		this.tableEmitterList[key].remove();
	}

}
