import {Component, OnInit, Input, TemplateRef, ContentChild, ViewChildren, HostListener, Output, EventEmitter} from '@angular/core';
import {Compare} from "./helpers/compare";
import {Pagination} from "./models/pagination";
import {TableService} from "./service/table.service";

declare var _:any;

export interface MassChangedEvent
{
	event:string;
}
export interface SortEvent
{
	sort:string;
}

@Component({
	selector: 'datatable',
	templateUrl: './table.component.html',
	styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit
{

	@Input()
	private name:string;
	@Input()
	private datasource:Object;
	@Input()
	private tableKey:string;
	@Input()
	private styleClass:string;
	@Input()
	private preview:string;
	@Input()
	private previewEnd:string;
	@Input()
	private previewMask:string;
	@Input()
	private previewPrefix:string;
	@Input()
	private attachedCustom:string;
	@Input()
	private fixedHeaders:boolean;
	@Input()
	private watcherComponent:boolean;
	@Input()
	private watcherSkipTopPixels:number;
	@Input()
	private customHeaders:string = '';
	@Input()
	private checkKeyChanges:Array<string> = [];
	@Input()
	private groupByKey:number;
	@Input()
	private sumByKey:number;
	@Input()
	private pagination:Pagination;
	@Input()
	private paginationMaxSize:number = 5;
	@Input()
	private massActions:Array<Object> = [];

	@Output()
	public massAction:EventEmitter<MassChangedEvent> = new EventEmitter<MassChangedEvent>(false);

	@Output()
	public sortAction:EventEmitter<SortEvent> = new EventEmitter<SortEvent>(false);

	public sort:Object = Object.create({});

	private datasourceExist:boolean = false;
	private dataExist:boolean = false;

	private fixedTableCredentials:Object = {};
	private showFixedHeader:boolean = false;
	private navbar = document.getElementById('nav-bar-component');
	private _topTableElementPosition = null;

	private _specialSource:any = false;
	private _specialSourceSum:number = 0;
	private _specialSourceCount:number = 0;

	private skipTopPixels:number = 0;
	private navbarHeight:number = 64;
	private _comparator:Compare;

	@ContentChild(TemplateRef)
	container;

	/**
	 * Listen filter elements of current component
	 */
	@ViewChildren('table')
	private table:any;
	@ViewChildren('tr')
	private tr:any;
	@ViewChildren('th')
	private th:any;
	@ViewChildren('trelement')
	private tr_element:any;
	@ViewChildren('headerTable')
	private headerTable:any;

	@HostListener('window:scroll', ['$event'])
	processScroll(event)
	{
		this.processFixedHeaders();
		this.processWatcher();
		if (this.customHeaders.length > 0) {
			this.processCustomHeaders();
		}
	}

	private get window()
	{
		return window;
	}

	public pageChanged(event:any):void
	{
		this.tableService.emitTable(this.tableKey, 'pageChanged', {
			'page': event.page,
			'per-page': event.itemsPerPage
		});
	};

	public changeSort(event:any):void
	{
		let sort: string = '';
		if (this.has(this.sort, event.column)) {
			switch (this.sort[event.column])
			{
				case 'none':
					sort = '';
					break;
				case 'desc':
					sort = '-' + event.column;
					break;
				case 'asc':
					sort = '' + event.column;
					break;
			}
		}

		this.sortAction.emit(
			{
				sort: sort
			});
	}

	private changeMassAction(event: any)
	{
		this.massAction.emit(
			{
				event: event
			});
	}

	private get windowYOffset()
	{
		return this.window['pageYOffset'];
	}

	private get headerTableClientHeight()
	{
		return this.has(this.headerTable.first, 'nativeElement') ? this.headerTable.first.nativeElement['clientHeight'] : 0;
	}

	get topTableElementPosition():number
	{
		return this._topTableElementPosition;
	}

	set topTableElementPosition(value:number)
	{
		this._topTableElementPosition = value;
	}

	get specialSource():any
	{
		return this._specialSource;
	}

	set specialSource(value:any)
	{
		this._specialSource = value;
	}

	get comparator():Compare
	{
		return this._comparator;
	}

	set comparator(value:Compare)
	{
		this._comparator = value;
	}

	get specialSourceSum():number
	{
		return this._specialSourceSum;
	}

	set specialSourceSum(value:number)
	{
		this._specialSourceSum = value;
	}

	get specialSourceCount():number
	{
		return this._specialSourceCount;
	}

	set specialSourceCount(value:number)
	{
		this._specialSourceCount = value;
	}

	constructor(private tableService:TableService)
	{
		this.comparator = new Compare();
	}

	ngOnInit()
	{
		this.processFixedHeaders();
	}

	ngAfterViewInit()
	{
		this.datasourceExist = typeof this.datasource != 'undefined' ? true : false;

		if (typeof this.table != 'undefined' && this.fixedHeaders == true) {
//			this.processFixedHeaders();
		}
		this.skipTopPixels = typeof this.watcherSkipTopPixels != 'undefined' ? this.watcherSkipTopPixels : 0;
		this.loadSort();

	}

	ngOnChanges(changes)
	{
		this.processFixedHeaders();
		this.processSelectedSum();
		this.loadSort();
	}

	private processWatcher()
	{
		try {
			if (typeof this.watcherComponent != 'undefined') {
				let self = this;
				_.forEach(this.watcherComponent, function (el)
				{
					if (self.has(self, el)) {
						self.showFixedHeader = self.windowYOffset - self.skipTopPixels - (self.getOffsetTop(self[el].first.nativeElement) - (self.has(self.navbar, 'offsetHeight') ? self.navbar.offsetHeight : 0)) > 0;
					}
				})
			}
		} catch (e) {

		}
	}

	private processFixedHeaders(recursive?:boolean)
	{
		try {
			if (typeof this.table != 'undefined' && this.fixedHeaders == true) {
				let table = this.table.first.nativeElement;
				this.fixedTableCredentials['table'] = table.offsetWidth;
				let thead = _.find(this.table.first.nativeElement.children, {nodeName: 'THEAD'});
				this.fixedTableCredentials['thead'] = thead.offsetWidth;
			}
		} catch (e) {

		}
	}

	private getOffsetTop(elem)
	{
		var offsetLeft = 0;
		let i = 0;
		do {
			if (!isNaN(elem.offsetTop)) {
				offsetLeft += elem.offsetTop;
			}
			i++;
		} while (elem = elem['offsetParent']);
		return offsetLeft;
	}

	private makeFixedStyle(i, type, skip:boolean = true)
	{
		let result = this.getFixedWidth(i, type);
		result['top'] = this.getTop(skip);
		return result;
	}

	private getTop(skip:boolean = true)
	{
		return (this.navbarHeight + (skip == true ? this.skipTopPixels : 0)) + 'px';
	}

	private loadSort()
	{

		if (this.has(this.datasource, 'headers')) {
			for (let i = 0; i < this.datasource['headers'].length; i++) {
//				console.log(this.datasource['headers'][i]);
				if (
					this.has(this.datasource['headers'][i], 'sort') &&
					this.datasource['headers'][i]['sort'] && !this.has(this.sort, this.datasource['headers'][i]['column'])
				) {
					this.sort[this.datasource['headers'][i]['column']] = 'none';
				}
			}
		}
//		this.sort[value] = 'none';
//		console.log(this.sort);
	}

	private processCustomHeaders()
	{
		this.getTopVisibleElementOfTable();
	}

	private processSelectedSum()
	{
		if (this.defined(this.sumByKey) && typeof this.datasource['extended'] != 'undefined') {
			let self = this;
			let sum = 0;
			_.each(this.datasource['extended']['selectedPositions'], function (el)
			{
				let obj = self.datasource['data'][el];
				let sumValue = _.find(obj, function (elem)
				{
					return self.has(elem, 'item') && elem.item['key'] == self.sumByKey;
				});
				if (self.has(sumValue, 'item')) {
					sum += parseFloat(sumValue['item']['value']);
				}
			});
			this.datasource['extended']['sum'] = sum;
		}
	}

	private getTopVisibleElementOfTable()
	{
		let self = this;
		let min = -10000;
		let minElementIndex = null;
		_.forEach(this.tr_element.toArray(), function (el, i)
		{
			let res = self.windowYOffset - self.getOffsetTop(el.nativeElement) + self.headerTableClientHeight - (self.has(self.navbar, 'offsetHeight') ? self.navbar.offsetHeight : 0);
			if (res > min && min < 0 && res < 0) {
				min = res;
				minElementIndex = i;
			}
		});

		if (minElementIndex != null) {
			this.topTableElementPosition = minElementIndex;
			if (this.has(this.datasource, 'data')) {
				if (this.has(this.datasource['data'], minElementIndex)) {
					let elem = this.datasource['data'][minElementIndex];
					let itemGroup = _.filter(elem, function (obj)
					{
						return self.has(obj, 'item') && obj.item['key'] == self.groupByKey;
					});
					if (this.defined(itemGroup[0]) && this.has(itemGroup[0], 'item')) {
						if (this.specialSourceCount != itemGroup[0]['item']['sum'] && this.specialSourceCount != itemGroup[0]['item']['count'] && this.has(itemGroup[0]['item'], 'sum')) {
							this.specialSourceCount = itemGroup[0]['item']['count'];
							this.specialSourceSum = itemGroup[0]['item']['sum'];
						}
						itemGroup[0]['item']['count'] = this.specialSourceCount;
						itemGroup[0]['item']['sum'] = this.specialSourceSum;
					}
					this.specialSource = elem;
				}
			}
		}
	}


	private calcSize(el, index)
	{
		if (this.fixedHeaders == true) {
			if (!this.has(this.fixedTableCredentials, 'th')) {
				this.fixedTableCredentials['th'] = [];
			}
			this.fixedTableCredentials['th'][index] = el.offsetWidth;
		}
	}

	private getFixedWidth(i, type)
	{
		let result = null;
		switch (true) {
			case i >= 0:
				result = typeof this.fixedTableCredentials[type] != 'undefined' && typeof this.fixedTableCredentials[type][i] != 'undefined' ? this.fixedTableCredentials[type][i] + 'px' : '0px';
				break;
			case i == -1 && type == 'table' && typeof this.fixedTableCredentials[type] != 'undefined':
				result = {
					'width': this.fixedTableCredentials[type] + 'px'
				};
				break;
			case  i == -1 && type == 'table' && typeof this.fixedTableCredentials[type] == 'undefined':
				result = {
					'width': '0px'
				};
				break;
		}
		return result;
	}

	private has(data:any, key:string)
	{
		return _.has(data, key) ? true : false;
	}

	private length(data)
	{
		return _.size(data);
	}

	private makeWidth(width)
	{
		return {
			'width': parseInt(width) ? width + '%' : ''
		};
	}

	private elementsHasField(value)
	{
		return this.defined(value[0]) && this.defined(value[0]['item']) && this.defined(value[0]['item']['customKey']);
	}

	private defined(value):boolean
	{
		return typeof value != 'undefined';
	}

	private getItemIndexByHeaderColName(key:string):number
	{
		return this.datasource['headers'].findIndex(x => x['column'] == key);
	}

}
