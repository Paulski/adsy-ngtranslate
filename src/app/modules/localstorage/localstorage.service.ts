import {Injectable} from '@angular/core';
import {LocalStorageService, IStorage} from "ng2-webstorage/index";

@Injectable()
export class LocalstorageService
{

	constructor(private storage:LocalStorageService){}

	public saveValue(key: string, value: any) {
		this.storage.store(key, value);
	}

	public getValue(key: string) {
		return this.storage.retrieve(key);
	}
}
