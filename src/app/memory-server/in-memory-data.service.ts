import {InMemoryDbService} from 'angular-in-memory-web-api';

export class InMemService implements InMemoryDbService
{
	createDb()
	{
		let get = [
			{
				name: 'translate-en.json',
				data: {
					"HELLO": "Hi",
					"Bie": "fc",
					"client_authorization": "Client Authorization",
					"buy_links_a_week": "Buy 3-5 Links a Week During 3 Months"
				}
			},
			{
				name: 'translate-ru.json',
				data: {
					"HELLO": "Привет",
					"client_authorization": "Авторизация Пользователя",
					"buy_links_a_week": "Покупайте 3-5 ссылок каждую неделю на протяжении 3 месяцев"
				}
			},
			{
				name: 'balance_transfer',
				data: {
					"balance_transfer": [
						{
							"transfer_id": 1,
							"transfer_time": 1475228137,
							"transfer_balance_id": 22374,
							"transfer_balance": 200,
							"transfer_aim: string": "order",
							"transfer_aim_id": 15,
							"transfer_amount": -50.00,
							"transfer_status": 0,
							"transfer_status_time": 0,
							"transfer_keyword": null,
							"transfer_project_id": 0,
							"transfer_code": 103,
							"transfer_source": 7,
							"transfer_txn_id": "0"
						},
						{
							"transfer_id": 2,
							"transfer_time": 1475228139,
							"transfer_balance_id": 22374,
							"transfer_balance": 150,
							"transfer_aim: string": "order",
							"transfer_aim_id": 15,
							"transfer_amount": -10.00,
							"transfer_status": 0,
							"transfer_status_time": 0,
							"transfer_keyword": null,
							"transfer_project_id": 0,
							"transfer_code": 103,
							"transfer_source": 7,
							"transfer_txn_id": "0"
						}
					]
				}
			}
		];

		let authenticated = [
			{
				name: 'authenticate',
				authenticated: true,
				user: {
					"buyer_id": "22374",
					"buyer_time_register": "1330568011",
					"buyer_time_login": "1330568217",
					"buyer_authkey": "",
					"buyer_name": "Marie",
					"buyer_last_name": "Curie",
					"buyer_email": "mariestay@hotmail.com",
					"buyer_password": "56436efd18cac",
					"buyer_password_new": "",
					"buyer_promo_code": "54bca850",
					"buyer_affiliate_id": "0",
					"buyer_affiliate_effect_id": "0",
					"buyer_affiliate_autotrans": "0",
					"buyer_status": "1",
					"buyer_notify_period": "0",
					"buyer_ads_host": "0",
					"buyer_comments": "",
					"buyer_confirm": "",
					"buyer_confirm_time": "0",
					"buyer_offer_period": "0",
					"buyer_login_count": "2",
					"buyer_last_entrance_ip": "1128672441",
					"buyer_credit_limit": "0",
					"buyer_debt_time": "0",
					"buyer_sweetoffer_send_time": "1339639758",
					"buyer_register_url": "http://www.linksmanagement.com/view/contextual_link_building_service?fwm02_21",
					"buyer_register_clear_url": "http://www.linksmanagement.com/view/contextual_link_building_service",
					"buyer_country": "Canada",
					"buyer_address": "",
					"buyer_payment_terms": "Maybe some time",
					"buyer_finance_plan": "",
					"buyer_phone": "555-658-1337",
					"buyer_phone_code": "",
					"buyer_zip": "",
					"buyer_package": "",
					"buyer_flb_package": "",
					"buyer_rw_report": "0",
					"buyer_link_per_week": "-1",
					"buyer_autobuy_mail_time": "0",
					"buyer_placement_percent": "0",
					"buyer_upload_funds": "0.00",
					"buyer_allow_manual": "0",
					"buyer_allow_issue_replace": "1",
					"buyer_history_mail_time": "0",
					"buyer_seoexpert_mail_time": "",
					"buyer_seoexpert_errmail_time": "",
					"buyer_skype": "",
					"buyer_referral_visited": "0",
					"buyer_sale": "0",
					"buyer_ref_notification": "0",
					"buyer_state": "6",
					"buyer_buy_links_page": "0",
					"buyer_all_unsubscribe": "0",
					"buyer_register_form": "",
					"buyer_last_utm_source": "",
					"buyer_last_utm_medium": "",
					"buyer_last_utm_campaign": "",
					"buyer_fb_id": "",
					"authkeys": [
						{
							"id": "1044796",
							"owner_id": "22374",
							"created": "1475746549",
							"value": "9c69761517cd2de0b937fd58cf64c369"
						}
					],
					"auth_key": "9c69761517cd2de0b937fd58cf64c369x22374",
					"admin": 1
				},
				token: '5aSd2Dsd1asd658s7afa8sf5a68s4fa5s1d',
				expired: 216000000

			},
		];

		let translations = [{
			name: 'translation',
			lang: 'en',
			category: 'dashboard',
			data: {
				"HELLO": "Привет",
				"client_authorization": "Авторизация Пользователя",
				"buy_links_a_week": "Покупайте 3-5 ссылок каждую неделю на протяжении 3 месяцев"
			}
		}];

		let fb = [
			{
				category_name: "Arts Education",
				country_iso: "IE",
				page_id: "2865700",
				page_language: "",
				page_param_ol: "15",
				page_param_pr: 0,
				page_param_sb: "1",
				page_rating: -1,
				page_seomoz_rank_autority: "17",
				page_url: "http://www.kidsmagician.ie/testimonials.html",
				page_user_cost: 0.24,
				site_country_id: 72,
				site_placement_percent: 75,
				site_seomoz_domain_authority: 11
			},
			{
				category_name: "Arts Education",
				country_iso: "IE",
				page_id: "2865700",
				page_language: "",
				page_param_ol: "15",
				page_param_pr: 1,
				page_param_sb: "1",
				page_rating: -1,
				page_seomoz_rank_autority: "17",
				page_url: "http://www.kidsmagician.ie/testimonials1.html",
				page_user_cost: 0.33,
				site_country_id: 99,
				site_placement_percent: 75,
				site_seomoz_domain_authority: 11
			},
			{
				category_name: "Arts Education",
				country_iso: "IE",
				page_id: "2865700",
				page_language: "",
				page_param_ol: "15",
				page_param_pr: 1,
				page_param_sb: "1",
				page_rating: -1,
				page_seomoz_rank_autority: "17",
				page_url: "http://www.kidsmagician.ie/testimonials2.html",
				page_user_cost: 0.48,
				site_country_id: 124,
				site_placement_percent: 75,
				site_seomoz_domain_authority: 11
			}
		]

		return {get, authenticated, translations, fb};
	}
}