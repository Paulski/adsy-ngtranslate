import {Injectable, Output, EventEmitter} from '@angular/core';
import {LocalStorageService, SessionStorageService} from 'ng2-webstorage';
import {language} from  '../../models/language.model';
import {TranslateService, TranslateLoader, TranslateStaticLoader} from '@paulski/ng2-translate';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {env} from "../../../environments/environment";

declare var $:any;

@Injectable()
export class LanguageService
{

	private _lang:language;
	public _laguageStack:Array<language>;
	public _isoList:Array<string> = [];
	private _defaultLang:language = {
		name: "English",
		iso: "en"
	};

	private _translations:Array<TranslateService> = [];

	constructor(private storage:LocalStorageService, private translate:TranslateService, private http:Http)
	{
		this._laguageStack = [
			{
				name: 'English',
				iso: 'en'
			},
			{
				name: 'Russian',
				iso: 'ru'
			}
		];

		this.checkLanguage();

		var self = this;
		this._laguageStack.forEach(function (item, i, arr)
		{
			self._isoList.push(item.iso);
		});
	}

	search(iso:string):any
	{
		var result = $.grep(this._laguageStack, function (e)
		{
			return e.iso == iso;
		});
		return result;
	}

	private saveLangInStorage(value:language)
	{
		if (typeof value != 'undefined') {
			this.storage.store('lang', value.iso);
			this.storage.store('lang:name', value.name);
		}
	}

	public getStoredLanguage()
	{
		return {
			iso: this.storage.retrieve('lang'),
			name: this.storage.retrieve('lang:name')
		}
	}

	/**
	 * Check language from LocalStorage
	 */
	private checkLanguage():void
	{
		switch (this.storage.retrieve('lang') == null) {
			case true:
				this.setLanguage(this._defaultLang.iso);
				break;
		}
	}

	/**
	 * Set language to LocalStorage
	 * @param lang
	 * @returns {boolean}
	 */
	public setLanguage(lang:string):boolean
	{
		let search = this.search(lang);
		if (typeof search[0].iso != 'undefined') {
			let self = this;
			let reload = false;
			let lengthTr = this._translations.length;
			this._translations.forEach(function (item, i, arr)
			{
				if(lengthTr == i+1)
				{
					reload = true;
				}
				item.setDefaultLang(lang);
				item.changeLanguage(self.http, env.API_TRANSLATION_URL+'category='+item.get_category(), lang, item.get_category(), reload);
			});
		}

		return true;
	}

	public translateFn(lang:string, component:string, translator)
	{
		translator.setDefaultLang(lang);
		translator.useCategory(component);
//		translator.changeLanguage(this.http, env.API_TRANSLATION_URL+'category='+component, lang, component);
		translator.addLangs(this._isoList);
		this._translations.push(translator);
	}

	public get_lang():language
	{
		return this._lang;
	}

	private set_lang(value:language):void
	{
		this._lang = value;
	}
}
