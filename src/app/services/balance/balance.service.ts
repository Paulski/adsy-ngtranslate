import {Injectable, EventEmitter} from '@angular/core';

import {IBalance} from '../../models/balance.model';
import {RestService} from "../../modules/http/rest.service";
import {FiltersService} from "../../modules/filters/services/filters.service";

@Injectable()
export class BalanceService
{

	private balance:Array<IBalance> = [];
	private balanceEmitter = new EventEmitter();

	constructor(private rest:RestService, private filterService:FiltersService)
	{
		this.filterService.subscribe(data => this.processFiltersEvents(data));
	}

	public getBalanceData()
	{
		let self = this;
		this.rest.balance.getBalance({}, function (data)
		{
			self.setBalance(data.json());
			self.emitBalance();
		})
	}

	public subscribe(onNext:(value:any) => void, onThrow?:(exception:any) => void, onReturn?:() => void)
	{
		return this.balanceEmitter.subscribe(onNext, onThrow, onReturn);
	}

	private emitBalance()
	{
		this.balanceEmitter.emit({});
	}

	private setBalance(balance:Array<IBalance>):void
	{
		this.balance = balance;
	}

	public getBalance():Array<IBalance>
	{
		return this.balance;
	}

	private processFiltersEvents(data)
	{
		let self = this;
		if (typeof data != 'undefined' && data.form == 'balance') {
			switch (true) {
				case (data.status.indexOf(['add']) != -1):
					break;
				case (data.status.indexOf(['changedFilter']) != -1):
                    let filters = this.filterService.processFiltersToObject('balance');
					this.rest.balance.getBalance(data['filters'], function (data)
					{
						self.setBalance(data.json());
                        self.emitBalance();
					});
					break;
			}
		}
	}
}
