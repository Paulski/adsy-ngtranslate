import {Injectable, EventEmitter} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {RestService} from "../../modules/http/rest.service";
import {LocalstorageService} from "../../modules/localstorage/localstorage.service";
import {IProject} from "../../models/project.model";
import {filtersForms} from '../../mock/forms/forms';
import {FiltersService} from "../../modules/filters/services/filters.service";

declare var _:any;

@Injectable()
export class ProjectsService
{

	private addProject = new EventEmitter();

	constructor(private http:Http, private rest:RestService, private localStorage:LocalstorageService, private filters:FiltersService)
	{
	}

	public subscribeOnAddProject(onNext:(value:any) => void, onThrow?:(exception:any) => void, onReturn?:() => void)
	{
		return this.addProject.subscribe(onNext, onThrow, onReturn);
	}

	public emitAddProject()
	{
		this.addProject.emit();
	}

	public addProjectData(project_url:string, callback:any)
	{
		this.rest.project.addProject({project_url: project_url}, data => callback(data));
	}

	public setLocalStorage(data:IProject)
	{
		let self = this;
		this.filters.getFiltersFromApi("balance", function (data)
		{
			self.filters.emitFilterLoad("balance");
			self.filters.emitFilterLoad("addtocartaddtocart_projects");
		}, {
			'balance': {
				replaceKey: 'addtocart',
				fields: ['projects', 'keywords']
			}
		});
	}

}
