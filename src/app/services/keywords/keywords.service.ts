import {Injectable, EventEmitter} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {RestService} from "../../modules/http/rest.service";
import {LocalstorageService} from "../../modules/localstorage/localstorage.service";
import {IKeyword} from "../../models/keyword.model";
import {filtersForms} from '../../mock/forms/forms';
import {FiltersService} from "../../modules/filters/services/filters.service";

declare var _: any;

@Injectable()
export class KeywordsService {

    private addKeyword = new EventEmitter();

    constructor(private http: Http, private rest: RestService, private localStorage: LocalstorageService, private filters: FiltersService) {
    }

    public subscribeOnAddKeyword(onNext:(value:any) => void, onThrow?:(exception:any) => void, onReturn?:() => void)
    {
        return this.addKeyword.subscribe(onNext, onThrow, onReturn);
    }

    public emitAddKeyword()
    {
        this.addKeyword.emit();
    }

    public addKeywordData(keyword_text:string, keyword_project_id: number, callback:any)
    {
        this.rest.keyword.addKeyword({keyword_text: keyword_text, keyword_project_id: keyword_project_id}, data => callback(data));
    }

    public setLocalStorage(data:IKeyword)
    {
        let self = this;
        this.filters.getFiltersFromApi("balance", function (data)
        {
            self.filters.emitFilterLoad("balance");
            self.filters.emitFilterLoad("addtocartaddtocart_keywords");
        }, {
            'balance': {
                replaceKey: 'addtocart',
                fields: ['projects', 'keywords']
            }
        });
    }

}
