import {Injectable, EventEmitter} from '@angular/core';
import {Link} from '../../models/link.model';
import {env} from '../../../environments/environment';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import {RestService} from "../../modules/http/rest.service";
import {FiltersService} from "../../modules/filters/services/filters.service";

declare var _:any;

@Injectable()
export class CartService
{

	private _sum:number = 0.00;
	private _quantity:number = 0;
	private _links:Link[] = [];
	private _linksId:string[] = [];
	private _openCartModalEvent = new EventEmitter();
	private _linkAlreadyAdded = new EventEmitter();
	private _cartChanged = new EventEmitter();
	private _cartLoaded = new EventEmitter();
	private _addtocartChanges = new EventEmitter();
	private _cart:Array<Object> = [];
	private _emitEvents:boolean = true;
	private _addedLinksIds:Array<number> = [];
	private _offersLoaded:boolean = false;

	private addFunds = new EventEmitter();
	private cartFiltersChanged = new EventEmitter();

	constructor(private http:Http, private rest:RestService, private filterService:FiltersService)
	{
		this.filterService.subscribe(data => this.processFiltersEvents(data));
	}

	private processFiltersEvents(data)
	{
		if (typeof data != 'undefined' && ['addtocart', 'cart'].indexOf(data['form']) != -1) {
			switch (true) {
				case (data.status.indexOf(['add']) != -1):

					break;
				case (data.status.indexOf(['changedFilter']) != -1) && data['form'] === 'addtocart':
					this.emitAddtocartChange(data);
					break;
				case (data.status.indexOf(['changedFilter']) != -1) && data['form'] === 'cart':
					this.emitCartFiltersChanged();
					break;
			}
		}
	}

	public subscribeOnModal(onNext:(value:any) => void, onThrow?:(exception:any) => void, onReturn?:() => void)
	{
		return this._openCartModalEvent.subscribe(onNext, onThrow, onReturn);
	}

	public subscribeOnCartFiltersChanged(onNext:(value:any) => void, onThrow?:(exception:any) => void, onReturn?:() => void)
	{
		return this.cartFiltersChanged.subscribe(onNext, onThrow, onReturn);
	}

	public subscribeOnAddFunds(onNext:(value:any) => void, onThrow?:(exception:any) => void, onReturn?:() => void)
	{
		return this.addFunds.subscribe(onNext, onThrow, onReturn);
	}

	public subscribeOnLinkAdd(onNext:(value:any) => void, onThrow?:(exception:any) => void, onReturn?:() => void)
	{
		return this._linkAlreadyAdded.subscribe(onNext, onThrow, onReturn);
	}

	public subscribeOnCart(onNext:(value:any) => void, onThrow?:(exception:any) => void, onReturn?:() => void)
	{
		return this._cartChanged.subscribe(onNext, onThrow, onReturn);
	}

	public subscribeOnCartload(onNext:(value:any) => void, onThrow?:(exception:any) => void, onReturn?:() => void)
	{
		return this._cartLoaded.subscribe(onNext, onThrow, onReturn);
	}

	public subscribeOnAddtocartChange(onNext:(value:any) => void, onThrow?:(exception:any) => void, onReturn?:() => void)
	{
		return this._addtocartChanges.subscribe(onNext, onThrow, onReturn);
	}

	public emitModalOpen()
	{
		this._openCartModalEvent.emit();
	}

	public emitCartFiltersChanged()
	{
		this.cartFiltersChanged.emit();
	}

	public emitAddfunds()
	{
		this.addFunds.emit();
	}

	private emitLinkAlreadyAdded()
	{
		this._linkAlreadyAdded.emit();
	}

	public emitCartChange()
	{
		this._cartChanged.emit();
	}

	public emitCartLoad()
	{
		this._cartLoaded.emit();
	}

	private emitAddtocartChange(data)
	{
		this._addtocartChanges.emit(data);
	}

	public addToSum(link:Link, emitEvents:boolean = true):void
	{
		this.setEventEmit(emitEvents);
		this.addLinks(link);
	}

	public addLinks(value:Link):void
	{
		this.processAddingLink(value);
	}

	private increment_quantity():void
	{
		this._quantity++;
	}

	private processAddingLink(link:Link):void
	{
		if (this.has(link, 'page_id')) {
			let condition = !this.linkExistById(link['page_id'], 'page_id');
			switch (true) {
				case condition:
					this._links.push(link);
					if (this.getEventEmit()) {
						this.emitModalOpen();
					}
					break;
				case !condition:
					if (this.getEventEmit()) {
						this.emitLinkAlreadyAdded();
					}
					break;
			}
		}
	}

	public convertOffersType(params:any)
	{
		this.rest.cart.convertOffers(params, data => this.afterConvert(data))
	}

	private afterConvert(data)
	{
		switch (true) {
			case data['status'] == true:
				this.afterOffersAdd({status: 'true'});
				break;
			case data['status'] == false:
				break;
		}
	}

	public linkExistById(id:string, colKey:string)
	{
		let self = this;
		let result = this.getLinks().filter(function (el)
		{
			return self.has(el, colKey) && el[colKey] == id.toString();
		});
		return result.length > 0 ? true : false;
	}

	private has(data:any, key:string)
	{
		return _.has(data, key) ? true : false;
	}

	public getLinksIdsOfAdded():any
	{
		var getKeys = function (obj)
		{
			var keys = [];
			for (var key in obj) {
				keys.push(obj[key]['page_id']);
			}
			return keys;
		}

		this.setAddedLinksIds(getKeys(this.getLinks()));
		return this.getAddedLinksIds();
	}

	public removeLinkById(id = 0):void
	{
		let list = this.getLinks();
		this.setLinks(list.filter(function (el)
		{
			return el['page_id'] != id.toString();
		}));
		this.emitCartChange();
	}

	public addOffers(credentials:Object):void
	{
		let offersList = this.makeOffers(credentials);

		this.rest.cart.addOffers(offersList, data => this.afterOffersAdd(data))
	}

	public afterOffersAdd(data)
	{
		switch (true) {
			case this.has(data, 'status') && data['status'].indexOf('true') != -1:
				this.loadOffers();
				this.clearLinks();
				this.emitCartChange();
				break;
		}
	}

	private makeOffers(credentials:Object):Array<Object>
	{
		let list = [];
		_.forEach(this.getLinks(), function (index, element)
		{
			list.push({
				offer_page_id: index['page_id'],
				text_before_anchor: credentials['text_before_anchor'],
				text_after_anchor: credentials['text_after_anchor'],
				project_url: credentials['project_url'],
				anchor: credentials['text_anchor'],
				keyword: credentials['keyword'],
				period: credentials['period'] ? 1 : 0,
				nofollow: credentials['nofollow']
			});
		});
		return list;
	}

	public loadOffers(params?:Object)
	{
		this.rest.cart.getOffers((typeof params != 'undefined' ? params : {}), data => this.afterOffersGet(data));
		this.loadTotalSum();
	}

	private loadTotalSum()
	{
		this.rest.cart.getTotalSum({}, data => this.processTotalSum(data));
	}

	private processTotalSum(data)
	{
		try {
			let result = data.json();
			if (this.defined(result['count']) && this.defined(result['sum'])) {
				this.setQuantity(result['count']);
				this.setSum(parseFloat(result['sum']));
			}
		} catch (e) {

		}
	}

	private defined(value):boolean
	{
		return typeof value != 'undefined';
	}

	public getOffers()
	{
		switch (true) {
			case this._offersLoaded:
				this.emitCartLoad();
				break;
			case !this._offersLoaded:
				this.loadOffers();
				break;
		}
	}

	private afterOffersGet(data)
	{
		data = data.json();
		if (_.size(data) > 0) {
			this.setCart(data);
			this.processCart();
			this._offersLoaded = true;
			this.emitCartLoad();
		}
	}

	private processCart()
	{
		//this.setQuantity(_.size(this.getCart()));
//		let sum = _.reduce(this.getCart(), function (s, entry)
//		{
//			let period = entry.offer_period == 0 ? 1 : 6;
//			return s + parseFloat(entry.offer_page_cost) * period;
//		}, 0);
		//this.setSum(parseFloat(sum));
	}

	public changeOffers(params:Object, callback: any)
	{
		this.rest.cart.changeLink(params, function (data)
		{
			callback(data);
		});
	}

	private clearLinks()
	{
		this._links = [];
	}

	private setLinks(links):void
	{
		this._links = links;
	}

	public getLinksId():string[]
	{
		return this._linksId;
	}

	public setLinksId(value:string)
	{
		this._linksId.push(value);
	}

	private setQuantity(quantity):void
	{
		this._quantity = quantity;
	}

	public get_quantity():number
	{
		return this._quantity;
	}

	public getLinks():Array<Link>
	{
		return this._links;
	}

	public getSum():number
	{
		return this._sum;
	}

	public setSum(value:number):void
	{
		this._sum = value;
	}

	private setCart(offers:Array<Object>)
	{
		this._cart = offers;
	}

	public getCart():Array<Object>
	{
		return this._cart;
	}

	private setEventEmit(value:boolean = true)
	{
		this._emitEvents = value;
	}

	public getEventEmit():boolean
	{
		return this._emitEvents;
	}

	private setAddedLinksIds(value:Array<number>)
	{
		this._addedLinksIds = value;
	}

	public getAddedLinksIds():Array<number>
	{
		return this._addedLinksIds;
	}
}
