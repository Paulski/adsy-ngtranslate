/* tslint:disable:no-unused-variable */

import {TestBed, async, inject} from '@angular/core/testing';
import {LinksService} from './links.service';

describe('Service: Links', () =>
{
	beforeEach(() =>
	{
		TestBed.configureTestingModule({
			providers: [LinksService]
		});
	});

	it('should ...', inject([LinksService], (service:LinksService) =>
	{
		expect(service).toBeTruthy();
	}));
});
