import {Injectable, EventEmitter} from "@angular/core";
import {ListLinks} from '../../models/link_collection.model';
import {Link} from '../../models/link.model';
import {HttpService} from "../../modules/http/http.service";
import {env} from "../../../environments/environment";
import {RestService} from "../../modules/http/rest.service";
import {filtersForms, filtersNames} from '../../mock/forms/forms'
import {FiltersService} from "../../modules/filters/services/filters.service";
import {Pagination} from "../../modules/table/models/pagination";

var parse = require('parse-link-header');

declare var _ : any;

@Injectable()
export class LinksService
{

	private links:any = [];
	private loadedFiltered = new EventEmitter();
	public pagination: Pagination = {
		total: 0,
		current: 0,
		perPage: 0,
		last: {},
		next: {},
		self: {}

	};

	constructor(private rest:RestService, private filterService:FiltersService)
	{
		// let self = this;
		// rest.fbModule.getFBLinks(function (data)
		// {
		//   self.setLink(data.json().data);
		//   self.emitLinkFiltered(true);
		// })

		this.filterService.subscribe(data => this.processFiltersEvents(data));
	}

	public loadLinks(form, filters:Object = {}, pagination?: Object):void
	{
		switch (true) {
			case (typeof form != 'undefined' && filtersNames().indexOf(form) != -1):
				let self = this;
				if(_.size(filters) == 0 && typeof pagination != 'undefined')
				{
					filters = this.filterService.processFiltersToObject(form);
					filters = Object.assign(filters, pagination);
				}
				this.rest.fbModule.getFBLinks(filters, function (data)
				{

					let links = data.headers;
					var linkHeader = links.get('link');
					var parsed = parse(linkHeader);
					self.pagination['total'] = parseInt(data.headers.get('x-pagination-total-count'));
					self.pagination.perPage = parseInt(data.headers.get('x-pagination-per-page'));
					self.pagination.current = parseInt(data.headers.get('x-pagination-current-page'));
					self.pagination = Object.assign(self.pagination, parsed);
					self.setLink(data.json(), form);
					self.emitLinkFiltered(true);
				});
				break;
		}
	}

	public processFiltersEvents(data)
	{
		if (typeof data != 'undefined' && data.form == 'fb') {
			switch (true) {
				case (data.status.indexOf(['add']) != -1):

					break;
				case (data.status.indexOf(['changedFilter']) != -1):
					this.loadLinks(data.form, data.filters, data.pagination);
					break;
			}
		}
	}

	public setLink(links:Link[], form:string)
	{
		this.links[form] = new ListLinks(links);
	}

	public getList(form:string)
	{
		return this.links[form].getAll();
	}

	public getLength(form:string):number
	{
		return this.links[form].size();
	}

	public isReady(form:string)
	{
		return (typeof this.links[form] != 'undefined') ? true : false;
	}

	public getLinkById(id:number, form:string):Link
	{
		return this.links[form].getLinksById([id]);
	}

	private emitLinkFiltered(success:boolean)
	{
		this.emitLinkFilteredEvent(success, null);
	}

	private emitLinkFilteredEvent(success:boolean, error:any)
	{
		this.loadedFiltered.emit(
			{
				success: success,
				error: error
			}
		);
	}

	public subscribe(onNext:(value:any) => void, onThrow?:(exception:any) => void, onReturn?:() => void)
	{
		return this.loadedFiltered.subscribe(onNext, onThrow, onReturn);
	}

}
