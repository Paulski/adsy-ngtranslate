/**
 * Modules
 */
import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule}   from '@angular/forms';
import {RouterModule} from '@angular/router';
import {MaterialModule} from '@angular/material';
import {HttpModule, JsonpModule}  from '@angular/http';

/**
 * Routes
 */
import {APP_ROUTER_PROVIDERS} from './app.routes';

/**
 * Plugin - Extensions
 */
import {
	Ng2BootstrapModule,
	DropdownModule,
	TabsModule,
	ModalModule,
	DatepickerModule
} from 'ng2-bootstrap/ng2-bootstrap';
import {TranslateModule, TranslateLoader, TranslateStaticLoader} from '@paulski/ng2-translate';
import {SidebarModule} from 'ng2-sidebar';

/**
 * Components
 */
import {AppComponent}  from './components/app/app.component';
import {DashboardComponent}  from './components/dashboard/dashboard.component';
import {BuyLinksService}  from './components/buy-links/buy-links.component';
import {BuyLinksSearchComponent}  from './components/buy-links/search/buy-links-search.component';
import {NavBarComponent}  from './components/nav-bar/nav-bar.component';
import {NavSidebarComponent} from './components/nav-sidebar/nav-sidebar.component';
import {CartComponent} from './components/cart/cart.component';
import {HowToComponent} from './components/dashboard/dropdowns/how-to/index';
import {AppFeaturesComponent} from './components/dashboard/dropdowns/app-features/index';
import {BalanceComponent} from './components/balance/balance.component';
import {SettingsComponent} from './components/settings/settings.component';
import {AddtocartComponent} from './components/addtocart/addtocart.component';
import {AddprojectComponent} from './components/addproject/addproject.component';
import {RecoveryComponent} from "./components/recovery/recovery.component";
import {AddfundsComponent} from "./components/addfunds/addfunds.component";
import {ChangeLinkComponent} from "./components/cartpage/helpers/changelink/changelink.component";
import {ChangeLinksGlobalComponent} from "./components/cartpage/helpers/change-links-global/change-links-global.component";
import {AddkeywordComponent} from './components/addkeyword/addkeyword.component';
/**
 * Services
 */
import {CartService} from "./services/shoping/cart.service";
import {LinksService} from "./services/links/links.service";
import {LanguageService} from './services/language/language.service';
import {BalanceService} from './services/balance/balance.service';
import {ProjectsService} from './services/projects/projects.service';
import {KeywordsService} from './services/keywords/keywords.service';

/**
 * Directives
 */

/**
 * Server Data-Storage
 */
import {InMemoryWebApiModule} from 'angular-in-memory-web-api';
import {InMemService}  from './memory-server/in-memory-data.service';

/**
 * Modules
 */

//Auth module
import {LoginComponent} from './components/login/login.component';
import {AuthService} from './modules/auth/services/auth/auth.service';
import {UsersService} from './modules/auth/services/user/users.service';
import {WindowService} from './modules/auth/services/window/window.service';
import {ProtectedDirective} from './modules/auth/directives/protected/protected.directive';
import {LoggedInGuard} from "./modules/auth/guards/loggedin.guard";
import {TextMaskModule} from 'angular2-text-mask';
import {MomentModule} from 'angular2-moment';

//HttpApiService
import {HttpService} from "./modules/http/http.service";
import {RestService} from "./modules/http/rest.service";

//Filters module
import {FiltersModule, FiltersComponent, InputsComponent} from "./modules/filters/index";

//Notification module
import {AlertService} from "./modules/notifications/alert/alert.service";
import {NumericPipe} from "./pipes/numeric/numeric.pipe";

// Custom html elements
import {SelectComponent} from './modules/custom_elements/select/select.component';
import {DateComponent} from './modules/custom_elements/date/date.component';

//Table Module
import {TableComponent} from "./modules/table/table.component";
import {CustomElementsComponent} from "./modules/table/helpers/custom-elements/custom-elements.component";
import {TableService} from "./modules/table/service/table.service";
import {CartpageComponent} from "./components/cartpage/cartpage.component";
import {CustomcartpageComponent} from "./components/cartpage/helpers/customcartpage/customcartpage.component";
import {TooltipDirective} from "./modules/table/helpers/directives/tooltip/tooltip.directive";
import {PaginationComponent} from "./modules/table/helpers/pagination/pagination.component";
import {SortComponent} from "./modules/table/helpers/sort/sort.component";
import {TableMassActionComponent} from "./modules/table/helpers/table-mass-action/table-mass-action.component";

//LocalStorageModule
import {LocalstorageService} from "./modules/localstorage/localstorage.service";

//AddFunds Module
import {AddFundsService} from "./modules/add-funds/services/add-funds.service";

import {Http, Headers, Request, Response, RequestOptions} from '@angular/http';
import {env} from "../environments/environment";

@NgModule({
	imports: [
		MaterialModule.forRoot(),
		BrowserModule,
		FormsModule,
		ReactiveFormsModule,
		RouterModule.forRoot(APP_ROUTER_PROVIDERS),
		DropdownModule,
		TranslateModule.forRoot(),
		SidebarModule,
		HttpModule,
		JsonpModule,
		TabsModule,
		ModalModule,
		DatepickerModule,
		TextMaskModule,
		MomentModule
	],
	providers: [
		HttpService,
		CartService,
		TableService,
		LinksService,
		LanguageService,
		BalanceService,
		RestService,
		AuthService,
		UsersService,
		AddFundsService,
		WindowService,
		LoggedInGuard,
		AlertService,
		LocalstorageService,
		ProjectsService,
		KeywordsService
	],
	declarations: [
		AppComponent,
		TooltipDirective,
		FiltersComponent,
		DashboardComponent,
		BuyLinksService,
		BuyLinksSearchComponent,
		LoginComponent,
		NavBarComponent,
		NavSidebarComponent,
		CartComponent,
		HowToComponent,
		AppFeaturesComponent,
		BalanceComponent,
		SettingsComponent,
		CartpageComponent,
		CustomcartpageComponent,
		InputsComponent,
		SelectComponent,
		DateComponent,
		AddtocartComponent,
		NumericPipe,
		TableComponent,
		TableMassActionComponent,
		SortComponent,
		CustomElementsComponent,
		AddprojectComponent,
		PaginationComponent,
		TableComponent,
		CustomElementsComponent,
		ChangeLinkComponent,
		ChangeLinksGlobalComponent,
		RecoveryComponent,
		AddfundsComponent,
		AddkeywordComponent,
	],
	exports: [
		TranslateModule,
		FiltersModule
	],
	bootstrap: [AppComponent],
	schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class AppModule
{

	constructor()
	{
	}
}
