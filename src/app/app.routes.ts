import {DashboardComponent} from './components/dashboard/dashboard.component';
import {BuyLinksService} from './components/buy-links/buy-links.component';
import {BalanceComponent} from './components/balance/balance.component';
import {LoggedInGuard} from "./modules/auth/guards/loggedin.guard";
import {SettingsComponent} from "./components/settings/settings.component";
import {CartpageComponent} from "./components/cartpage/cartpage.component";
import {RecoveryComponent} from "./components/recovery/recovery.component";

export const routes:any = [
	{
		path: 'dashboard',
		component: DashboardComponent,
		canActivate: [LoggedInGuard]
	},
	{
		path: 'buy-links',
		component: BuyLinksService,
		canActivate: [LoggedInGuard]
	},
	{
		path: 'balance',
		component: BalanceComponent,
		canActivate: [LoggedInGuard]
	},
	{
		path: 'settings',
		component: SettingsComponent,
		canActivate: [LoggedInGuard]
	},
	{
		path: 'cart',
		component: CartpageComponent,
		canActivate: [LoggedInGuard]
	},
	{
		path: 'recovery',
		component: RecoveryComponent
	},
	{
		path: '',
		redirectTo: '/dashboard',
		pathMatch: 'full'
	},
];

export const APP_ROUTER_PROVIDERS = routes;

/*
 Copyright 2016 Google Inc. All Rights Reserved.
 Use of this source code is governed by an MIT-style license that
 can be found in the LICENSE file at http://angular.io/license
 */