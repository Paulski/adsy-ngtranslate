import {Component, OnInit} from '@angular/core';
import {AuthService, ISignInForm} from "../../modules/auth/services/auth/auth.service";
import {IAuthWatcher} from "../../modules/auth/model/IAuthWatcher.model";
import {Router} from '@angular/router';
import {EmailValidator} from "../../modules/auth/directives/validators/email/emailvalidator.directive";
import {TranslateService, TranslateLoader, TranslateStaticLoader} from '@paulski/ng2-translate';
import {Http} from '@angular/http';
import {LanguageService} from "../../services/language/language.service";
import {UsersService} from "../../modules/auth/services/user/users.service";
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {AlertService} from "../../modules/notifications/alert/alert.service";
import {env} from "../../../environments/environment";
import {LoggedInGuard} from "../../modules/auth/guards/loggedin.guard";


@Component({
	selector: 'app-modal',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css'],
	viewProviders: [EmailValidator],
	providers: [
		{
			provide: TranslateLoader,
			useFactory: (http:Http) => new TranslateStaticLoader(http, env.API_TRANSLATION_URL+'category=login_page', 'en'),
			deps: [Http]
		},
		TranslateService,
		UsersService
	]
})
export class LoginComponent implements OnInit
{

	private session:any;
	private authStatus:boolean = true;
	private loginForm:FormGroup;
	private registrationForm:FormGroup;
	private is_formToggled:boolean = false;
	private forgotPassword: boolean = false;
	private forgotSteps: number = 1;
	private showAuth: boolean = true;

	private fieldsError: Object =
	{
		email: false,
		password: false
	};

	private fieldsRegisterError: Object =
	{
		reg_email: false,
		reg_password: false,
		reg_password_confirmation: false,
	};

	private userCredentials:ISignInForm =
	{
		email: "",
		password: "",
		rememberMe: 0,
	};

	constructor(private fb:FormBuilder, private guard: LoggedInGuard, private alert: AlertService, private authService:AuthService, private router:Router, public translate:TranslateService, public languageService:LanguageService, private user:UsersService)
	{
		const emailNoRegex = `^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$`;

		this.loginForm = this.fb.group({
			email: [{value: '', disabled: false}, [Validators.pattern(emailNoRegex), Validators.required]],
			password: [{value: '', disabled: false}, Validators.required],
			rememberMe: [{value: '', disabled: false}],
		});
		
		this.registrationForm = this.fb.group({
			reg_email: [{value: '', disabled: false}, [Validators.pattern(emailNoRegex), Validators.required]],
			reg_password: [{value: '', disabled: false}, Validators.required],
			reg_password_confirmation: [{value: '', disabled: false}, Validators.required],
		});

		this.authService.subscribeByAuthenticated(data => this.checkAuth(data))
		this.authService.subscribeByErrorRegister(data => this.errorRegister(data))

		//Load language
		let lang = languageService.getStoredLanguage().iso;
		languageService.translateFn(lang, 'sign-in', this.translate);
		
		if(this.guard._navRoute.indexOf(['/recovery']) != -1)
		{
			this.showAuth = false;
		}
	}

	ngOnInit()
	{
		this.session = this.authService.getSession();
	}

	authenticateStatusCheck(value:IAuthWatcher)
	{
		if (value.authenticated == false) {
			this.authStatus = true;
		}
	}
	
	private errorRegister(data)
	{
		this.alert.alert({
			title: this.translate.get(['oops'])['value']['oops'],
			text: this.translate.get(['wrong_authorization_credentials_please_try_again'])['value']['wrong_authorization_credentials_please_try_again'],
			type: 'error'
		});
	}
	
	private sendRecovery(email: string)
	{
		let self = this;
		this.authService.recoveryPassword({email: email}, function (data)
		{
			if(data == true)
			{
				self.forgotSteps = 2;
			}
		});
	}

	doLogin(form, valid): void
	{
		switch (true)
		{
			case (valid == true):
				this.auth();
				break;
			case (valid == false):

//				let auth = this.translate.get(['client_authorization']);

				for(let fieldIndex in this.fieldsError)
				{
					let field = typeof form.controls[fieldIndex] != 'undefined' ? form.controls[fieldIndex] : false;
					if(field)
					{
						switch (true)
						{
							case (field.touched == false):
								this.fieldsError[fieldIndex] = true;
								break;
						}
					}
				}
				break;
		}
	}

	doRegister(form, valid): void
	{
		switch (true)
		{
			case (valid == true && form.controls.reg_password.value == form.controls.reg_password_confirmation.value):
				this.register();
			break;
			case (valid == false):
				for(let fieldIndex in this.fieldsRegisterError)
				{
					let field = typeof form.controls[fieldIndex] != 'undefined' ? form.controls[fieldIndex] : false;
					if(field)
					{
						switch (true)
						{
							case (field.touched == false):
								this.fieldsRegisterError[fieldIndex] = true;
							break;
						}
					}
				}
			break;
			case (form.controls.reg_password.value != form.controls.reg_password_confirmation.value):
				this.fieldsRegisterError['reg_password'] = true;
				this.fieldsRegisterError['reg_password_confirmation'] = true;
			break;
		}
	}

	private auth()
	{
		this.authService.doAuth(this.getCredentials());

		if(this.authService.isAuthenticated())
		{
			this.authStatus = false;
		}
	}
	
	private register()
	{
		this.authService.doRegister(this.getRegisterCredentials());
	}

	private getCredentials()
	{
		return this.loginForm.value;
	}

	private getRegisterCredentials()
	{
		return this.registrationForm.value;
	}

	private checkAuth(data)
	{
		if(this.authService.isAuthenticated())
		{
			this.authStatus = false;
		} else
		{
			this.authStatus = true;
		}
	}

	private logout()
	{
		this.authService.doLogout();
		this.session = this.authService.getSession();
		this.authStatus = true;
	}

}
