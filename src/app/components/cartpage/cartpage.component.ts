import {Component, OnInit, EventEmitter, ViewChild} from '@angular/core';
import {CartService} from "../../services/shoping/cart.service";
import {TableService} from "../../modules/table/service/table.service";
import {tableMaker} from "../../modules/table/helpers/tableMaker";
import {NumericPipe} from "../../pipes/numeric/numeric.pipe";
import {LocalstorageService} from "../../modules/localstorage/localstorage.service";

declare var _:any;
declare var $:any;

@Component({
	selector: 'cartpage',
	templateUrl: './cartpage.component.html',
	styleUrls: ['./cartpage.component.css']
})
export class CartpageComponent implements OnInit
{
	private offersList:Array<Object> = [];
	private datasource:Object = {};
	private dataLoadedEvent = new EventEmitter();
	private numericPipe:NumericPipe = new NumericPipe();
	private _anchor:Object = {};
	private _project:string = '';
	private _keyword:string = '';
	private attachedCustom:Array<Object> = [];

	private _currentSort:string;

	private sourceListAvailable:Object = {
		fields: [
			{
				title: 'backlink_page_url',
				column: 'offer_page_url',
				class: 'break-word',
				width: "15"
			},
			{
				title: 'anchor',
				custom: {
					type: 'event_with_preview',
					valueColumn: 'offer_id',
					value: 'offer_anchor_text',
					event: 'changeAnchor',
					preview: {
						prefix: 'offer_text_before_anchor',
						end: 'offer_text_after_anchor'
					}
				},
				width: "25"
			},
			{
				title: 'sb_rank',
				column: 'offer_page_sb',
				width: "10",
				sort: true
			},
			{
				title: 'pr',
				column: 'offer_page_pr',
				width: "5",
				sort: true
			},
			{
				title: 'project_url',
				column: 'offer_project_url',
				width: "10",
				special: "customcartpage",
				key: 'domain'
			},
			{
				title: 'keyword',
				column: 'offer_keyword_text',
				width: "10",
				special: "customcartpage",
				key: 'keyword'
			},
			{
				title: 'Link type',
				custom: {
					type: 'radiogroup_two',
					valueColumn: 'offer_id',
					value: 'offer_period',
					event: 'changelinktype'
				},
				width: "10"
			},
			{
				title: 'cost',
				column: 'offer_page_cost',
				width: "10",
				factor: 6,
				factorColumn: 'offer_period',
				pipe: {
					class: this.numericPipe,
					arguments: {type: 'decimal', value: 2}
				},
				key: 'cost',
				sort: true
			},
			{
				title: 'Select',
				custom: {
					type: 'checkbox',
					valueColumn: 'offer_id',
					value: 'offer_checked',
					event: 'selectlink'
				},
				width: "5"
			}

		],
		groupByKey: 'domain',
		sumByKey: 'cost',
		key: {
			column: 'page_id'
		},
		tableClass: "",
		customHeaders: 'customcartpage',
		checkKeyChanges: ['domain'],
		selectedEvent: 'selectlink',
		massActions: [
			{
				name: 'Convert to Monthly',
				event: 'toMonthly'
			},
			{
				name: 'Convert to Permanent',
				event: 'toPermanent'
			},
			{
				name: 'Set other Project / Keyword / Anchor',
				event: 'setOtherUrl'
			},
		]
//		checkKeyChanges: ['domain', 'keyword']
	};

	private tableKey:string = 'cartOffers';

	@ViewChild('ChangeLink')
	private ChangeLink;
	@ViewChild('ChangeLinkGlobal')
	private ChangeLinkGlobal;
	@ViewChild('ProjectFilter')
	public projectFilter;
	@ViewChild('KeywordFilter')
	public keywordFilter;

	private filterLoaded:boolean = false;

	private events = {};

	constructor(private cart:CartService, private table:TableService, private storage:LocalstorageService)
	{
		this.events['registerTable'] = this.table.registerTable(this.tableKey).subscribe(this.tableKey, data => this.processEvent(data));
		this.events['cartLoad'] = this.cart.subscribeOnCartload(data => this.loadOffers(data));
		this.cart.getOffers();
		this.events['filterChanged'] = this.cart.subscribeOnCartFiltersChanged(data => this.processCartFiltersChanged(data));
	}

	private processCartFiltersChanged(data:{})
	{
		//very important parameter - did available to prepare filters of table, else filters not working
		this.filterLoaded = true;

		let projectId = this.projectFilter.getValue();
		let keywordId = this.keywordFilter.getValue();
		let params = {};
		if (typeof projectId != 'undefined') {
			params['project_id'] = projectId;
		}
		if (typeof keywordId != 'undefined') {
			params['keyword_id'] = keywordId;
		}

		if (typeof this.currentSort != 'undefined') {
			params['sort'] = this.currentSort;
		}

		this.cart.loadOffers(params);
	}

	get anchor():Object
	{
		return this._anchor;
	}

	set anchor(value:Object)
	{
		this._anchor = value;
	}

	get project():string
	{
		return this._project;
	}

	set project(value:string)
	{
		this._project = value;
	}

	get keyword():string
	{
		return this._keyword;
	}

	set keyword(value:string)
	{
		this._keyword = value;
	}

	get currentSort():string
	{
		return this._currentSort;
	}

	set currentSort(value:string)
	{
		this._currentSort = value;
	}

	ngOnInit()
	{
	}

	ngAfterViewInit()
	{
		$("html, body").animate({scrollTop: 0}, "slow");
	}

	private processEvent(data)
	{
		switch (true) {
			case data['event'] == 'changeAnchor':
				this.processChangeAnchorData(data);
				break;
			case data['event'] == 'changelinktype':
				this.processChangeLinkType(data);
				break;
		}
//		this.makeAttachedCustom();
	}

	private processChangeAnchorData(data:any) {
		let self = this;
		this.filterLoaded = true;
		this.anchor = data['element'];
		let element = _.find(this.getOffers(), function (elem) {
			return self.has(elem, 'offer_id') && elem.offer_id == data['element']['item'];
		});

		let projectURL = this.has(element, 'offer_project_url') ? element['offer_project_url'] : "";

		let projectObj = this.storage.getValue('filter_addtocart_projects');
		let projectSearched = {};
		if (this.has(projectObj, 'value')) {
			projectSearched = _.find(projectObj['value'], function (elem) {
				let index = Object.keys(elem)[0];
				return elem[index] === projectURL;
			});
		}
		let keywordText = this.has(element, 'offer_keyword_text') ? element['offer_keyword_text'] : "";

		let keywordObj = this.storage.getValue('filter_addtocart_keywords');
		let keywordSearched = {};
		if (this.has(keywordObj, 'value')){
				keywordSearched = _.find(keywordObj['value'], function (elem) {
				let index = Object.keys(elem)[0];
				return elem[index] === keywordText;
			});
		}

		this.project = projectSearched != null ? Object.keys(projectSearched)[0] : '';
		this.keyword = keywordSearched != null ? Object.keys(keywordSearched)[0] : '';
		this.anchor['id'] = this.has(element, 'offer_anchor_id') ? element['offer_anchor_id'] : "";
		this.ChangeLink.showChildModal();
	}

	private has(data:any, key:string)
	{
		return _.has(data, key);
	}

	private processChangeLinkType(data:any)
	{
		if (typeof data['element'] != 'undefined') {
			this.filterLoaded = true;
			let params =
			{
				offer_ids: [data['element']['item']],
				offer_period: data['element']['value']
			};
			this.cart.convertOffersType(params);
		}
	}

	private loadOffers(data):void
	{
		this.setOffers(this.cart.getCart());
		if (_.size(this.datasource) == 0 || this.filterLoaded) {
			this.makeDataSource();
			this.filterLoaded = false;
		}
		this.makeAttachedCustom();
	}

	private makeDataSource():void
	{
		this.datasource = new tableMaker(this.getOffers(), this.sourceListAvailable, this.sourceListAvailable['checkKeyChanges'], this.sourceListAvailable['groupByKey'], this.sourceListAvailable['sumByKey'], this.sourceListAvailable['selectedEvent']).getDataSource();
	}

	private setOffers(value:Array<Object>):void
	{
		this.offersList = Object.assign({}, value);
	}

	private getOffers():Array<Object>
	{
		return this.offersList;
	}

	private massActionProcessor(event:any)
	{
		if (this.has(event, 'event')) {
			let params = {};
			switch (event['event']['event']) {
				case 'toMonthly':
					this.filterLoaded = true;
					params =
					{
						offer_ids: this.getLoadedOffersIds(),
						offer_period: 0
					};
					this.cart.convertOffersType(params);
					break;
				case 'toPermanent':
					this.filterLoaded = true;
					params =
					{
						offer_ids: this.getLoadedOffersIds(),
						offer_period: 1
					};
					this.cart.convertOffersType(params);
					break;
				case 'setOtherUrl':
					this.ChangeLinkGlobal.showChildModal();
					break;
			}
		}
	}

	private actionSort(event:any):void
	{
		if (this.has(event, 'sort')) {
			this.currentSort = event['sort'];
			this.processCartFiltersChanged({});
		}
	}

	private getLoadedOffersIds()
	{
		var offerKeys = [];
		for (let item in this.offersList) {
			offerKeys.push(this.offersList[item]['offer_id']);
		}
		return offerKeys;
	}

	private getPermanentOffersIds()
	{
		var offerKeys = [];
		for (let item in this.offersList) {
			if (this.offersList[item]['offer_period'] == 1) {
				offerKeys.push(this.offersList[item]['offer_id']);
			}
		}
		return offerKeys;
	}

	private makeAttachedCustom()
	{
		this.attachedCustom = [{
			data: this.getPermanentOffersIds(),
			processEvents: {
				'changelinktype': true
			}
		}];
	}

	private changeLinks(event:any)
	{
		var offerKeys = [];
		let self = this;
		for (let item in this.offersList) {
			offerKeys.push(this.offersList[item]['offer_id']);
		}

		this.cart.changeOffers({
			project_id: this.has(event, 'project_id') ? event['project_id'] : '',
			keyword_id: this.has(event, 'keyword_id') ? event['keyword_id'] : '',
			anchor_title: this.has(event, 'anchor_title') ? event['anchor_title'] : '',
			offer_ids: offerKeys
		}, function (data)
		{
			switch (data['status'])
			{
				case true:
					self.filterLoaded = true;
					self.ChangeLinkGlobal.childModal.hide();
					self.cart.afterOffersAdd({'status':  'true'});
					break;
				case false:
					console.log('wrong data');
					break;
			}
		});
	}

	ngOnDestroy(): void
	{
		for (let item in this.events) {
			this.events[item].unsubscribe();
		}
	}

}
