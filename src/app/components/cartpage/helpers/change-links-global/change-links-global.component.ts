import {Component, OnInit, ViewChild, Input, Output, EventEmitter} from '@angular/core';
import {ModalDirective} from 'ng2-bootstrap/ng2-bootstrap';
import {RestService} from "../../../../modules/http/rest.service";

declare var $ : any;
@Component({
	selector: 'changeLinksGlobal',
	templateUrl: './change-links-global.component.html',
	styleUrls: ['./change-links-global.component.css']
})
export class ChangeLinksGlobalComponent implements OnInit
{

	@ViewChild('childModal')
	public childModal:ModalDirective;
	@ViewChild('Project')
	public projectFilter;
	@ViewChild('Keyword')
	public keywordFilter;
	@ViewChild('Anchor')
	public anchorFilter;

	@Output()
	public changeLinks:EventEmitter<any> = new EventEmitter<any>(false);

	constructor()
	{
	}

	ngOnInit()
	{
	}

	public showChildModal():void
	{
		this.childModal.config.backdrop=false;
		this.childModal.show();
	}

	public hideChildModal():void
	{
		this.childModal.hide();
	}

	private change()
	{
		let result = {};
		let project = this.projectFilter.getValue();
		let keyword = this.keywordFilter.getValue();
		let anchor = this.anchorFilter.nativeElement.value;
		if(project.length > 0)
		{
			result['project_id'] = project;
		}
		if(keyword.length > 0)
		{
			result['keyword_id'] = keyword;
		}
		if(anchor.length > 0)
		{
			result['anchor_title'] = anchor;
		}
		this.changeLinks.emit(result);
	}

}
