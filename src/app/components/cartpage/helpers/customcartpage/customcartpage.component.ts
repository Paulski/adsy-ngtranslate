import {Component, OnInit, Input} from '@angular/core';

declare var _:any;

@Component({
	selector: 'customcartpage',
	templateUrl: './customcartpage.component.html',
	styleUrls: ['./customcartpage.component.css']
})
export class CustomcartpageComponent implements OnInit
{

	@Input()
	private specialSource:any;
	@Input()
	private style:any;
	@Input()
	private position:any;
	@Input()
	private class:string;
	@Input()
	private extended:Object;

	private _datasource:Object = {
		domain: null,
		keyword: null
	};

	get datasource():Object
	{
		return this._datasource;
	}

	set datasource(value:Object)
	{
		this._datasource = value;
	}

	constructor()
	{
	}

	ngOnInit()
	{
	}

	ngOnChanges(changes)
	{
		this.extractProcessedData();
	}

	ngAfterViewInit()
	{
		this.extractProcessedData();
		if (this.defined(this.position) && this.position.length > 0) {
			switch (this.position) {
				case 'fixed':
					this.style = Object.assign({'position': this.position, 'z-index': '100'}, this.style);
					break;
				case 'relative':
					this.style = Object.assign({'position': this.position, 'z-index': '1'}, this.style);
					break;
			}
		}
	}

	private extractProcessedData()
	{
		let has = this.has;
		let source = this.specialSource.filter(function (el)
		{
			return has(el, 'item') && el.item['special'] == 'customcartpage';
		});
		this.datasource['domain'] = _.find(this.specialSource, function (obj)
		{
			return has(obj, 'item') && obj.item['key'] == 'domain';
		});
		this.datasource['keyword'] = _.find(this.specialSource, function (obj)
		{
			return has(obj, 'item') && obj.item['key'] == 'keyword';
		});
	}

	private has(data:any, key:string)
	{
		return _.has(data, key) ? true : false;
	}

	private defined(value):boolean
	{
		return typeof value != 'undefined';
	}

	private extractDomain(url)
	{
		var domain;
		if (url.indexOf("://") > -1) {
			domain = url.split('/')[2];
		}
		else {
			domain = url.split('/')[0];
		}
		domain = domain.split(':')[0];

		return domain;
	}

	private length(data)
	{
		return _.size(data);
	}

	private getLocation(href)
	{
		var l = document.createElement("a");
		l.href = href;
		return l;
	};

}
