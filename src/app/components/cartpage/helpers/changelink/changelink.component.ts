import {Component, OnInit, ViewChild, Input} from '@angular/core';
import {ModalDirective} from 'ng2-bootstrap/ng2-bootstrap';
import {RestService} from "../../../../modules/http/rest.service";
import {AlertService} from "../../../../modules/notifications/alert/alert.service";
import {CartService} from "../../../../services/shoping/cart.service";

declare var $ : any;

@Component({
	selector: 'changelink',
	templateUrl: './changelink.component.html',
	styleUrls: ['./changelink.component.css']
})
export class ChangeLinkComponent implements OnInit
{

	@ViewChild('childModal')
	public childModal:ModalDirective;
	@ViewChild('Project')
	public project;
	@ViewChild('Keyword')
	public keyword;

	@Input()
	private anchor:Object;
	@Input()
	private projectId:string = '1';
	@Input()
	private keywordId:string = '1';

	private _isChanged: boolean = false;

	constructor(private rest:RestService, private alert: AlertService, private cart: CartService)
	{
	}

	get isChanged():boolean
	{
		return this._isChanged;
	}

	set isChanged(value:boolean)
	{
		this._isChanged = value;
	}

	ngOnInit()
	{
	}

	ngAfterViewInit()
	{
	}

	ngOnChanges()
	{
		this.project.checkSelected();
	}

	private change()
	{
		this.isChanged = true;
	}

	private checkChanges()
	{
		if(this.projectId != this.project.getValue() || this.keywordId != this.keyword.getValue())
		{
			this.change();
		}
	}

	private update()
	{
		this.checkChanges();

		let params = {
			offer_ids: [parseInt(this.anchor['item'])],
			keyword_id: parseInt(this.keyword.getValue()),
			project_id: parseInt(this.project.getValue()),
		};

		switch (true)
		{
			case this.isChanged:
				params['anchor_title'] = this.anchor['value'];
				break;
			default:
				params['anchor_id'] = parseInt(this.anchor['id']);
				break;
		}

		let self = this;
		this.rest.cart.changeLink(params, function(data)
		{
			switch (data['status'])
			{
				case true:
					self.hideChildModal();
					self.cart.afterOffersAdd({'status':  'true'});
					break;
				case false:
					self.alert.alert({
						type: 'warning',
						title: 'Oops',
						text: 'Please check all input data'
					});
					break;
			}
		});
	}

	public showChildModal():void
	{
		this.childModal.config.backdrop=false;
		this.childModal.show();
	}

	public hideChildModal():void
	{
		this.childModal.hide();
	}

	ngOnDestroy()
	{
	}

}
