import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {LanguageService} from "../../services/language/language.service";
import {language} from "../../models/language.model";
import {AuthService} from "../../modules/auth/services/auth/auth.service";
import {NavBarComponent} from "../nav-bar/nav-bar.component";
import {UsersService} from "../../modules/auth/services/user/users.service";
import {CartService} from "../../services/shoping/cart.service";

@Component({
	selector: 'app-side-bar',
	templateUrl: './nav-sidebar.component.html',
	styleUrls: ['./nav-sidebar.component.css']
})
export class NavSidebarComponent implements OnInit
{

	languages:Array<Object>;
	currentLangIso:string;
	success:EventEmitter<{results:boolean}>;
	private user: any;

	constructor(private languageService:LanguageService, private auth:AuthService, user:UsersService, private cartService: CartService)
	{
		this.languages = this.languageService._laguageStack;
		this.success = new EventEmitter<{results:boolean}>();
		this.currentLangIso = this.languageService.getStoredLanguage().iso;
		this.user = user.get_user();
	}

	setLanguage(query:any)
	{
		query = query.target.value;
		let results = this.languageService.setLanguage(query);
		this.success.next({results: true});
	}

	ngOnInit()
	{
	}

	public items:Array<string> = ['Amsterdam', 'Antwerp', 'Athens', 'Barcelona',
		'Berlin', 'Birmingham', 'Bradford', 'Bremen', 'Brussels', 'Bucharest',
		'Budapest', 'Cologne', 'Copenhagen', 'Dortmund', 'Dresden', 'Dublin',
		'Düsseldorf', 'Essen', 'Frankfurt', 'Genoa', 'Glasgow', 'Gothenburg',
		'Hamburg', 'Hannover', 'Helsinki', 'Kraków', 'Leeds', 'Leipzig', 'Lisbon',
		'London', 'Madrid', 'Manchester', 'Marseille', 'Milan', 'Munich', 'Málaga',
		'Naples', 'Palermo', 'Paris', 'Poznań', 'Prague', 'Riga', 'Rome',
		'Rotterdam', 'Seville', 'Sheffield', 'Sofia', 'Stockholm', 'Stuttgart',
		'The Hague', 'Turin', 'Valencia', 'Vienna', 'Vilnius', 'Warsaw', 'Wrocław',
		'Zagreb', 'Zaragoza', 'Łódź'];

	private value:any = {};
	private _disabledV:string = '0';
	private disabled:boolean = false;

	private get disabledV():string
	{
		return this._disabledV;
	}
	
	private toggleAddFunds()
	{
		this.cartService.emitAddfunds();
	}

	private set disabledV(value:string)
	{
		this._disabledV = value;
		this.disabled = this._disabledV === '1';
	}

	public selected(value:any):void
	{
		console.log('Selected value is: ', value);
	}

	public removed(value:any):void
	{
		console.log('Removed value is: ', value);
	}

	public typed(value:any):void
	{
		console.log('New search input: ', value);
	}

	public refreshValue(value:any):void
	{
		this.value = value;
	}

	private logout()
	{
		this.auth.doLogout();
	}

	private _toggleSidebar()
	{
	}

}
