import {Component, enableProdMode, AfterViewInit, ViewContainerRef} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {env} from '../../../environments/environment';
import {AuthService} from "../../modules/auth/services/auth/auth.service";
import {Router} from "@angular/router";

enableProdMode();

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent
{

	private session:any;

	public title = 'sasdas';
	private viewContainerRef:ViewContainerRef;

	constructor(viewContainerRef:ViewContainerRef, private auth:AuthService, private router:Router)
	{
		this.viewContainerRef = viewContainerRef;
		this.session = this.auth.getSession();
		this.auth.subscribe((value) => this.authenticateStatusCheck(value));
	}

	ngOnInit()
	{
	}

	authenticateStatusCheck(value)
	{

		this.session = this.auth.getSession();
	}

}