import {Component, OnInit} from '@angular/core';

import {BalanceService} from '../../services/balance/balance.service';
import {IBalance} from "../../models/balance.model";
import {TableService} from "../../modules/table/service/table.service";
import {tableMaker} from "../../modules/table/helpers/tableMaker";

import * as moment from 'moment';

@Component({
    selector: 'app-balance',
    templateUrl: './balance.component.html',
    styleUrls: ['./balance.component.css']
})
export class BalanceComponent implements OnInit {

    public balance: Array<IBalance> = [];

    private sourceListAvailable: Object = {
        fields: [
            {
                title: 'date',
                column: 'transfer_time'
            },
            {
                title: 'transaction_id',
                column: 'transfer_id'
            },
            {
                title: 'source_type',
                column: 'source_title'
            },
            {
                title: 'information',
                column: 'code_name'
            },
            {
                title: 'operation',
                column: 'transfer_amount'
            },
            {
                title: 'balance',
                column: 'transfer_user_balance'
            },
        ],
        key: {
            column: 'transfer_id'
        },
        tableClass: "balance-table"
    };

    private datasource: Object = {};

    private tableKey: string = 'balance';

    constructor(private balance_service: BalanceService, private table: TableService) {
        this.balance_service.subscribe(data => this.proccessBalance(data));
        this.balance_service.getBalanceData();
        this.table.registerTable('balance').subscribe('balance', data => this.processEvent(data));
    }

    private proccessBalance(data) {
        this.balance = this.balance_service.getBalance();
        console.log(this.balance);
        this.makeDataSource();
    }

    ngOnInit() {
    }

    private processEvent(data) {
        console.log(data);
    }

    private makeDataSource() {
        this.datasource = new tableMaker(this.balance, this.sourceListAvailable).getDataSource();
    }

}
