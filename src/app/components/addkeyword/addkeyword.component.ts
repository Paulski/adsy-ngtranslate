import {Component, OnInit, ViewChild} from '@angular/core';
import {ModalDirective} from 'ng2-bootstrap/ng2-bootstrap';
import {KeywordsService} from "../../services/keywords/keywords.service"
import {FiltersService} from "../../modules/filters/services/filters.service";

declare var _: any;

@Component({
    selector: 'addkeyword',
    templateUrl: './addkeyword.component.html',
    styleUrls: ['./addkeyword.component.css']
})
export class AddkeywordComponent implements OnInit {

    @ViewChild('addKeywordModal')
    public childModal: ModalDirective;

    private openEvent: any;

    constructor(private keyword: KeywordsService, private filters: FiltersService) {
        this.openEvent = this.keyword.subscribeOnAddKeyword(data => this.openKeywordModal(data));
    }

    ngOnInit() {
    }

    public showChildModal(): void {
        this.childModal.show();
    }

    public hideChildModal(): void {
        this.childModal.hide();
    }

    private openKeywordModal(data: any) {
        this.showChildModal();
    }

    private addKeyword(keyword_text: string): void {
        let self = this;
        let filter_addtocart = this.filters.getFilters('addtocart');

        filter_addtocart = _.find(filter_addtocart, function(obj) {
            if(obj['name'] == 'project_url') {
                return true;
            }
        });
        let keyword_project_id = filter_addtocart.getValue();
        if(filter_addtocart.getValue() > 0) {
            this.keyword.addKeywordData(keyword_text, keyword_project_id, function (data) {
                self.keyword.setLocalStorage(data);
                self.hideChildModal();
            });
        }

    }

}
