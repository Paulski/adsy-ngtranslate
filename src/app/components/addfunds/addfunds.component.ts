import {Component, ViewChild, OnInit} from '@angular/core';
import {ModalDirective} from 'ng2-bootstrap/ng2-bootstrap';
import {CartService} from "../../services/shoping/cart.service";
import {AddFundsService} from "../../modules/add-funds/services/add-funds.service";
import {AlertService} from "../../modules/notifications/alert/alert.service";

declare var _:any;

@Component({
	selector: 'addfunds',
	templateUrl: './addfunds.component.html',
	styleUrls: ['./addfunds.component.css']
})
export class AddfundsComponent implements OnInit
{

	@ViewChild('childModal')
	public childModal:ModalDirective;

	private _images:Array<string> = [];
	private paymentTypes = {
		main: [],
		other: []
	};
	private _selectedItem:Object;
	private _step:number = 1;

	get images():Array<string>
	{
		return this._images;
	}

	set images(value:Array<string>)
	{
		this._images = value;
	}

	get selectedItem():Object
	{
		return this._selectedItem;
	}

	set selectedItem(value:Object)
	{
		this._selectedItem = value;
	}

	get step():number
	{
		return this._step;
	}

	set step(value:number)
	{
		this._step = value;
	}

	constructor(private cartService:CartService, private addFundsService:AddFundsService, private alert:AlertService)
	{
		this.cartService.subscribeOnAddFunds(data => this.toggleModal(data));
	}

	ngOnInit()
	{
	}

	private toggleModal(data):void
	{
		this.paymentTypes = this.addFundsService.getPaymentTypes();
		this.setFirstStep();
		this.childModal.toggle();
	}

	public showChildModal():void
	{
		this.childModal.show();
	}

	public hideChildModal():void
	{
		this.childModal.hide();
	}

	private changeType(type, value:string):void
	{
		switch (true) {
			case type.indexOf('main') != -1 && parseInt(value) >= 0:
				this.selectedItem = this.paymentTypes[type][value];
				break;
			case type.indexOf('main') != -1 && value.indexOf('other') != -1:
				this.selectedItem = {};
				break;
		}

		this.setFirstStep();
	}

	private setFirstStep()
	{
		this.step = 1;
	}

	private has(data:any, key:string):boolean
	{
		return _.has(data, key) ? true : false;
	}

	private length(data):number
	{
		return _.size(data);
	}

	private bringToFirstStep():void
	{
		this.step = 1;
	}

	private sendToNextStep():void
	{
		if (this.has(this.selectedItem, 'form') && this.length(this.selectedItem['form']['step']) > this.step) {
			this.step++;
		}
	}

	private bringToPrevStep():void
	{
		if (this.step > 1) {
			this.step--;
		}
	}

	private buttonClick(value:string):void
	{
		switch (value) {
			case 'next':
				this.sendToNextStep();
				break;
			case 'back':
				this.bringToPrevStep();
				break;
			case 'submit':
				this.submitForm();
				break;
		}
	}

	private submitForm():void
	{
		let result = this.addFundsService.processPaymentSubmit(this.selectedItem);
		this.alert.alert((result ? {
			type: 'success',
			title: 'Payment',
			text: 'Success Payment'
		} : {
			type: 'warning',
			title: 'Payment',
			text: 'Wrong data in form of credentials'
		}));
	}

}
