import {Component, OnInit} from '@angular/core';
import {TranslateService, TranslateLoader, TranslateStaticLoader} from '@paulski/ng2-translate';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {env} from '../../../environments/environment';
import {LanguageService} from "../../services/language/language.service";
import {WindowService} from "../../modules/auth/services/window/window.service";
import {AuthService} from "../../modules/auth/services/auth/auth.service";

@Component({
	selector: 'my-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.css'],
	providers: [
		{
			provide: TranslateLoader,
			useFactory: (http:Http) => new TranslateStaticLoader(http, env.API_TRANSLATION_URL+'category=dashboard', 'en'),
			deps: [Http]
		},
		TranslateService
	]
})
export class DashboardComponent implements OnInit
{

	constructor(public translate:TranslateService, public languageService:LanguageService, public windows:WindowService, private authService:AuthService)
	{
		let lang = languageService.getStoredLanguage().iso;
		languageService.translateFn(lang, 'dashboard', this.translate);
	}

	ngOnInit()
	{

	}

	logger(value)
	{
	}

}