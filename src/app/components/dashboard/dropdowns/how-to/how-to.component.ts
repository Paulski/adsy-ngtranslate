import {Component, OnInit} from '@angular/core';

@Component({
	selector: 'app-how-to',
	templateUrl: './how-to.component.html',
	styleUrls: ['./how-to.component.css']
})
export class HowToComponent implements OnInit
{

	public disabled:boolean = false;
	public status:{isopen:boolean} = {isopen: false};
	public items:Array<string> = ['The first choice!',
		'And another choice for you.', 'but wait! A third!'];

	constructor()
	{
	}

	ngOnInit()
	{
	}

	public toggled(open:boolean):void
	{
		console.log('Dropdown is now: ', open);
	}

	public toggleDropdown($event:MouseEvent):void
	{
		$event.preventDefault();
		$event.stopPropagation();
		this.status.isopen = !this.status.isopen;
	}

}

