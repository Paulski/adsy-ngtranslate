import {Component, OnInit, EventEmitter} from '@angular/core';
import {CartService} from '../../../services/shoping/cart.service';
import {LinksService} from '../../../services/links/links.service';
import {TranslateService, TranslateLoader, TranslateStaticLoader} from '@paulski/ng2-translate';
import {Link} from '../../../models/link.model';
import {filters} from '../../../mock/filters/filters.mock';
import {FiltersModule} from "../../../modules/filters/index";
import {filtersForms} from '../../../mock/forms/forms'
import {AlertService} from "../../../modules/notifications/alert/alert.service";
import {Http} from '@angular/http';
import {TableService} from "../../../modules/table/service/table.service";
import {tableMaker} from "../../../modules/table/helpers/tableMaker";
import {NumericPipe} from "../../../pipes/numeric/numeric.pipe";
import {env} from "../../../../environments/environment";
import {LanguageService} from "../../../services/language/language.service";

declare var _ : any;

@Component({
	selector: 'router-filter',
	templateUrl: './buy-links-search.component.html',
	styleUrls: ['./buy-links-search.component.css'],
	providers: [
		{
			provide: TranslateLoader,
			useFactory: (http:Http) => new TranslateStaticLoader(http, env.API_TRANSLATION_URL+'category=buy-links-search', 'en'),
			deps: [Http]
		},
		TranslateService
	]
})
export class BuyLinksSearchComponent implements OnInit
{

	public cart:any;
	public balance:number;
	public linksList:any;
	public link:Link;
	private form:string;
	private filters:Object;
	private datasource:Object = {};
	private eventTableSource = new EventEmitter();
	private attachedCustom:Array<Object> = [];
	private advancedToggle: boolean = false;
	private arrowsAdvancedToggle: Object = {
		true: 'keyboard_arrow_up',
		false: 'keyboard_arrow_down'
	};
	
	private sourceListAvailable:Object = {
		fields: [
			{
				title: 'backlink_page_url',
				column: 'page_url',
				class: 'break-word',
				width: "30",
				sort: true
			},
			{
				title: 'sb_rank',
				column: 'page_param_sb',
				width: "10",
				sort: true
			},
			{
				title: 'da',
				column: 'site_seomoz_domain_authority',
				width: "5",
				class: "none",
				sort: true
			},
			{
				title: 'pa',
				column: 'page_seomoz_rank_autority',
				width: "5",
				class: "none",
				sort: true
			},
			{
				title: 'percent',
				column: 'site_placement_percent',
				width: "10",
				class: "none",
				sort: true
			},
			{
				title: 'ol',
				column: 'page_param_ol',
				width: "10",
				class: "none",
				sort: true
			},
			{
				title: 'price_per_month',
				column: 'page_user_cost',
				width: "10",
				sort: true
			},
			{
				title: 'one_time_price',
				column: 'page_user_cost',
				width: "10",
				factor: 6,
				pipe: {
					class: new NumericPipe(),
					arguments: {type: 'decimal', value: 2}
				}
			},
			{
				title: 'Action',
				custom: {
					type: 'icon',
					valueColumn: 'page_id',
					imgLink: 'add_shopping_cart',
					event: 'addtocart',
					imgToggle: 'done'
				},
				width: "5"
			},
			{
				title: 'Select',
				custom: {
					type: 'checkbox',
					valueColumn: 'page_id',
					imgLink: 'add_shopping_cart',
					event: 'togleaddtocart'
				},
				width: "5"
			}
		],
		key: {
			column: 'page_id'
		},
		tableClass: ""
	};
	private tableKey:string = 'buyLinksSearch';
	private tableSubscription = null;
	private cartSubscription = null;

	constructor(private shoping:CartService, private links:LinksService, private alert:AlertService, private translate:TranslateService, private table:TableService, private languageService: LanguageService)
	{
		let lang = languageService.getStoredLanguage().iso;
		languageService.translateFn(lang, 'buy-links-search', this.translate);

		this.form = filtersForms['fb'].name;
		this.links.loadLinks(this.form);
		this.balance = this.shoping.getSum();
		if (this.links.isReady(this.form)) {
			this.linksList = this.links.getList(this.form);
		}
		this.filters = filters;
	}

	ngOnInit()
	{

		this.shoping.subscribeOnLinkAdd(data => this.showAlertOfAlreadyAddedLink(data));

		this.tableSubscription = this.table.registerTable(this.tableKey).subscribe(this.tableKey, data => this.processEvent(data));
		this.cartSubscription = this.shoping.subscribeOnCart(data => this.processEvent({'event': 'reloadCart'}));
		this.links.subscribe((data) => this.publicFilterResult(data))
	}

	ngOnDestroy()
	{
		this.tableSubscription.unsubscribe();
		this.cartSubscription.unsubscribe();
	}

	private processEvent(data)
	{
		switch (true) {
			case data['event'] == 'addtocart':
				this.link = this.links.getLinkById(data['element'], this.form);
				let linksExistAdd = false;
				if (typeof this.link != 'undefined') {
					linksExistAdd = this.shoping.linkExistById(this.link['page_id'], 'page_id');
				}
				switch (true) {
					case linksExistAdd:
						this.shoping.emitModalOpen();
						break;
					case !linksExistAdd:
						this.shoping.addToSum(this.link, true);
						break;
				}
				break;
			case data['event'] == 'togleaddtocart':
				this.link = this.links.getLinkById(data['element'], this.form);
				let linksExist = false;
				if (typeof this.link != 'undefined') {
					linksExist = this.shoping.linkExistById(this.link['page_id'], 'page_id');
				}
				switch (true) {
					case linksExist:
						this.shoping.removeLinkById(parseInt(this.link['page_id']));
						break;
					case !linksExist:
						this.shoping.addToSum(this.link, false);
						break;
				}
				this.makeAttachedCustom();
				break;
			case data['event'] == 'reloadCart':
				break;
			case data['event'] == 'pageChanged':
				let params = {};
				params['status'] = 'changedFilter';
				params['form'] = filtersForms['fb'].name;
				params = Object.assign(params, {pagination: data['element']});
				this.links.processFiltersEvents(params);
				break;
		}
		this.makeAttachedCustom();
	}

	private makeDataSource():void
	{
		this.datasource = new tableMaker(this.linksList, this.sourceListAvailable).getDataSource();
	}

	private publicFilterResult(data)
	{
		this.linksList = this.links.getList(this.form);
		this.makeDataSource();
		this.makeAttachedCustom();
	}

	private makeAttachedCustom()
	{
		this.attachedCustom = [{
			data: this.shoping.getLinksIdsOfAdded(),
			processEvents: {
				'addtocart': true,
				'togleaddtocart': true
			}
		}];
	}

	private buyLink(linkID:number)
	{
		this.link = this.links.getLinkById(linkID, this.form);
		this.shoping.addToSum(this.link);
	}

	private remove(index:number):void
	{
		if (index > -1) {
			this.linksList.splice(index, 1);
		}
	}

	private showAlertOfAlreadyAddedLink(data)
	{
		this.alert.alert({
			title: this.translate.get(['oops'])['value']['oops'],
			text: this.translate.get(['this_link_already_added'])['value']['this_link_already_added'],
			type: 'error'
		});
	}
	
	private toggleAdvanced()
	{
		this.advancedToggle = this.advancedToggle == true ? false : true;
	}
}
