"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var common_1 = require("@angular/common");
var http_1 = require('@angular/http');
var datatable_1 = require('angular2-datatable/datatable');
var BuyLinksService = (function () {
    function BuyLinksService(http, activatedRoute) {
        var _this = this;
        this.http = http;
        this.activatedRoute = activatedRoute;
        this.recordsTotal = 1;
        this.counter = 25;
        this.sortByWordLength = function (a) {
            return a.name.length;
        };
        var response;
        var q = http.get('/get-links').map(function (response) {
                return response.json();
            })
            .subscribe(function (data) {
                _this.data = data.data;
                _this.recordsTotal = data.recordsTotal;
            });
    }

    BuyLinksService.prototype.ngAfterViewInit = function () {
    };
    BuyLinksService.prototype.ngOnInit = function () {
    };
    BuyLinksService.prototype.plainValueChanged = function (event, container) {
        var el = this.getElement(container);
        el.innerText = event.startValue;
    };
    BuyLinksService.prototype.getElement = function (data) {
        if (typeof (data) == 'string') {
            return document.getElementById(data);
        }
        if (typeof (data) == 'object' && data instanceof Element) {
            return data;
        }
        return null;
    };
    BuyLinksService.prototype.removeItem = function (item) {
        this.data = _.filter(this.data, function (elem) {
            return elem != item;
        });
        console.log("Remove: ", item.page_url);
    };
    BuyLinksService = __decorate([
        core_1.Component({
            selector: 'buy-links',
            templateUrl: 'app/view/buy-links.component.html',
            styleUrls: ['app/public/css/style.css'],
            providers: [http_1.HTTP_BINDINGS],
            directives: [datatable_1.DataTableDirectives, router_1.ROUTER_DIRECTIVES],
            pipes: [common_1.DatePipe]
        }),
        __metadata('design:paramtypes', [http_1.Http, router_1.ActivatedRoute])
    ], BuyLinksService);
    return BuyLinksService;
}());
exports.BuyLinksService = BuyLinksService;
//# sourceMappingURL=buy-links.component.js.map