import {Component, OnInit} from '@angular/core';
import {CartService} from '../../services/shoping/cart.service';
import {Router} from '@angular/router';

@Component({
	selector: 'app-cart',
	templateUrl: './cart.component.html',
	styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit
{

	private cartUrl: string = '/cart';

	constructor(public cart:CartService, private router:Router)
	{
	}

	ngOnInit()
	{
		this.cart.subscribeOnCart(data => this.processCartChange(data));
		this.cart.getOffers();
	}

	private processCartChange(data)
	{
//		this.cart.getOffers();
	}

	private goToCart()
	{
		this.router.navigate([this.cartUrl]);
	}

}
