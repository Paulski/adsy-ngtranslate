import {Component, OnInit, ViewChild} from '@angular/core';
import {ModalDirective} from 'ng2-bootstrap/ng2-bootstrap';
import {ProjectsService} from "../../services/projects/projects.service";

declare var _: any;

@Component({
    selector: 'addproject',
    templateUrl: './addproject.component.html',
    styleUrls: ['./addproject.component.css']
})
export class AddprojectComponent implements OnInit {

    @ViewChild('addProjectModal')
    public childModal: ModalDirective;

    private openEvent: any;

    constructor(private project: ProjectsService) {
        this.openEvent = this.project.subscribeOnAddProject(data => this.openProjectModal(data));
    }

    ngOnInit() {
    }

    public showChildModal(): void {
        this.childModal.show();
    }

    public hideChildModal(): void {
        this.childModal.hide();
    }

    private openProjectModal(data: any) {
        this.showChildModal();
    }

    private addProject(project_url: string): void {
        let self = this;
        this.project.addProjectData(project_url, function (data) {
            self.project.setLocalStorage(data);
            self.hideChildModal();
        });
    }

}
