import {Component, ViewChild, OnInit} from '@angular/core';
import {ModalDirective} from 'ng2-bootstrap/ng2-bootstrap';
import {CartService} from "../../services/shoping/cart.service";
import {tableMaker} from "../../modules/table/helpers/tableMaker";
import {TableService} from "../../modules/table/service/table.service";
import {ProjectsService} from "../../services/projects/projects.service";
import {KeywordsService} from "../../services/keywords/keywords.service";

declare var _:any;

@Component({
	selector: 'addtocart',
	templateUrl: './addtocart.component.html',
	styleUrls: ['./addtocart.component.css'],
})
export class AddtocartComponent implements OnInit
{
	@ViewChild('childModal')
	public childModal:ModalDirective;
	private datasource:Object = {};

	private previewPrefix:string = "";
	private previewEnd:string = "";
	private previewMask:string = "";
	private nofollow:boolean = false;
	private period:boolean = false;
	private formAddToCart = {
		"project_url": "",
		"keyword": "",
	};

	private sourceListAvailable:Object = {
		fields: [
			{
				title: 'Backlink Page URL',
				column: 'page_url',
				class: 'break-word',
				width: "40"
			},
			{
				title: 'SBRank',
				column: 'page_param_sb',
				width: "20"
			},
			{
				title: 'Price',
				column: 'page_user_cost',
				width: "20"
			},
			{
				title: 'Action',
				custom: {
					type: 'icon',
					valueColumn: 'page_id',
					imgLink: 'delete_sweep',
					event: 'delete'
				},
				width: "20"
			}
		],
		key: {
			column: 'page_id'
		},
		tableClass: "inline-y-scroll-300",
		preview: {
			type: 'link',
			column: 'page_url',
			position: 'center',
			availableVar: ['previewPrefix', 'previewEnd']
		}
	};
	private tableKey:string = 'addtocart';

	constructor(private cart:CartService, private table:TableService, private project:ProjectsService, private keyword:KeywordsService)
	{
		this.cart.subscribeOnModal(data => this.openCartModal(data));
		this.table.registerTable('addtocart').subscribe('addtocart', data => this.processEvent(data));
		this.cart.subscribeOnAddtocartChange(data => this.processAddtocartChange(data));
		this.makeDataSource();
	}

	private processAddtocartChange(data)
	{
		this.formAddToCart['project_url'] = this.has(data['filters'], 'project_id') ? data['filters'].project_id : '';
		this.formAddToCart['keyword'] = this.has(data['filters'], 'keyword_id') ? data['filters'].keyword_id : '';
	}

	private processEvent(data)
	{
		switch (true) {
			case this.has(data, 'event') && data['event'] == 'delete':
				this.cart.removeLinkById(data['element']);
				this.makeDataSource();
				break;
		}
	}

	public showChildModal():void
	{
		this.childModal.show();
	}

	public hideChildModal():void
	{
		this.childModal.hide();
	}

	ngOnInit()
	{
	}

	private emit()
	{
	}

	private addOffers():void
	{
		console.log(this.formAddToCart);
		if(this.formAddToCart['project_url'].length > 0 && this.formAddToCart['keyword'].length > 0 )
		{
			this.cart.addOffers({
				'text_before_anchor': this.previewPrefix,
				'text_after_anchor': this.previewEnd,
				'text_anchor': this.previewMask,
				'period': this.period,
				'project_url': this.formAddToCart['project_url'],
				'keyword': this.formAddToCart['keyword'],
				'nofollow': this.nofollow,
			});
			this.childModal.hide();
			this.makeDataSource();
		}
	}

	private addProject():void
	{
        this.project.emitAddProject();
	}

	private addKeyword():void
	{
        this.keyword.emitAddKeyword();
	}

	private has(data:any, key:string)
	{
		return _.has(data, key) ? true : false;
	}

	private openCartModal(data)
	{
		this.makeDataSource();
		this.showChildModal();
	}

	private makeDataSource()
	{
		this.datasource = new tableMaker(this.cart.getLinks(), this.sourceListAvailable).getDataSource();
	}

	private toggle(variable:boolean):boolean
	{
		return variable == true ? false : true;
	}

}
