import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../modules/auth/services/auth/auth.service";
import {UsersService} from "../../modules/auth/services/user/users.service";
import {IUserModel} from "../../modules/auth/model/user.model";

@Component({
	selector: 'app-nav-bar',
	templateUrl: './nav-bar.component.html',
	styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit
{

	private _open:boolean = false;
	private user:IUserModel;

	constructor(user:UsersService)
	{
		this.user = user.get_user();
	}

	ngOnInit()
	{
	}

	public _toggleSidebar()
	{
		this._open = !this._open;
	}

}
