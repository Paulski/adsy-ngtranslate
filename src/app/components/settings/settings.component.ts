import {Component, OnInit, EventEmitter} from '@angular/core';
import {FormGroup, FormArray, FormBuilder, FormControl, Validators} from '@angular/forms';
import {HttpService} from "../../modules/http/http.service";
import {IUserModel} from "../../modules/auth/model/user.model";
import {AuthService} from "../../modules/auth/services/auth/auth.service";
import {UsersService} from "../../modules/auth/services/user/users.service";

declare var $:any;

@Component({
	selector: 'app-settings',
	templateUrl: './settings.component.html',
	styleUrls: ['./settings.component.css'],
	providers: [HttpService]
})
export class SettingsComponent implements OnInit
{

	public settingsForm:FormGroup;
	public credentialsForm:FormGroup;
	public countryEvent = new EventEmitter();
	private country: any = {};
	private user:IUserModel;
	private phoneCodes:Array<number> = [1, 7, 20, 27, 30, 31, 32, 33, 34, 36, 38, 39, 40, 41, 43, 44, 45, 46, 47, 48, 49,
	51, 52, 53, 54, 55, 56, 57, 58, 60, 61, 62, 63, 64, 65, 66, 81, 82, 84, 86, 90, 91, 92, 93, 94, 95, 98, 212,
	213, 216, 218, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237,
	238, 239, 240, 241, 242, 242, 243, 244, 245, 246, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 256,
	257, 258, 260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 284, 284, 290, 291, 297, 298, 299, 345, 350,
	351, 352, 353, 354, 355, 356, 357, 358, 359, 370, 371, 372, 373, 374, 375, 376, 378, 380, 381, 381, 385,
	386, 387, 389, 420, 421, 423, 473, 500, 501, 502, 503, 504, 505, 506, 507, 508, 509, 591, 592, 593, 594,
	595, 596, 596, 597, 598, 599, 670, 671, 672, 673, 674, 675, 676, 677, 678, 679, 680, 681, 682, 683, 684,
	685, 686, 687, 688, 689, 690, 691, 692, 767, 809, 809, 850, 850, 852, 853, 855, 855, 856, 869, 869, 876,
	880, 886, 886, 960, 961, 962, 963, 964, 965, 966, 967, 968, 971, 972, 973, 974, 975, 976, 977, 993, 994,
	995, 996, 1340, 1670, 1787, 1868];

	constructor(private fb:FormBuilder, private query:HttpService, private auth:AuthService, private userService: UsersService)
	{
		this.auth.subscribeByAuthenticated(data => this.processUser(data));
		this.processUser({});
		this.countryEvent.subscribe(data => this.processCountryResponse(data));
	}


	public subscribe(onNext:(value:any) => void, onThrow?:(exception:any) => void, onReturn?:() => void)
	{
		return this.countryEvent.subscribe(onNext, onThrow, onReturn);
	}

	private processCountryResponse(data)
	{
		this.setCountry(data[0]['name']);
	}
	
	ngOnInit()
	{
		const emailNoRegex = `\\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}`;
		this.setCountry(this.user['country']);

		this.settingsForm = this.fb.group({
			user_first_name: [{value: this.user['first_name'], disabled: false}, Validators.required],
			user_last_name: [{value: this.user['last_name'], disabled: false}, Validators.required],
			user_skype: [{value: this.user['skype_id'], disabled: false}],
			email: [{value: this.user['email'], disabled: true}, Validators.pattern(emailNoRegex)],
			user_phone_code: [{value: this.user['phone_code'], disabled: false}],
			user_phone: [{value: this.user['phone'], disabled: false}],
		});

		this.credentialsForm = this.fb.group({
			current_password: [{value: '', disabled: false}, Validators.required],
			new_password: [{value: '', disabled: false}, Validators.required],
			repeat_new_password: [{value: '', disabled: false}, Validators.required]
		});
	}

	doLogin(form, valid)
	{
		event.preventDefault();
		if(valid)
		{
			let parameters = form.value;
			parameters['user_country'] = this.country;
			parameters['user_phone_code'] = parseInt(parameters['user_phone_code']);
			console.log(parameters);
			this.userService.updateUser(parameters);
		}
	}

	doChangePassword(form, valid)
	{
		event.preventDefault();
		if(valid)
		{
			let parameters = form.value;
			switch (true)
			{
				case this.aboveNull(parameters['current_password']) && this.aboveNull(parameters['new_password']) && this.aboveNull(['repeat_new_password']):

					if(parameters['new_password'] == parameters['repeat_new_password'])
					{
						this.userService.changePassword(parameters);
					}
					break;
			}
		}
	}
	
	private aboveNull(val)
	{
		return val.length > 0;
	}

	private setCountry(data: string)
	{
		this.country = data;
	}

	private processUser(data)
	{
		this.user = this.auth.getUserInfo();
	}

}
