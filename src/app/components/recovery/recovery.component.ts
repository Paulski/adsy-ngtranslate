import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {AuthService} from "../../modules/auth/services/auth/auth.service";
import {LoggedInGuard} from "../../modules/auth/guards/loggedin.guard";
import {AlertService} from "../../modules/notifications/alert/alert.service";

@Component({
	selector: 'app-recovery',
	templateUrl: './recovery.component.html',
	styleUrls: ['./recovery.component.css']
})
export class RecoveryComponent implements OnInit
{

	private forgotSteps:number = 2;
	private queryParams:Object = {};

	constructor(private route:ActivatedRoute, private router:Router, private authService:AuthService, private alert: AlertService)
	{
		let self = this;
		this.route.queryParams.subscribe(params =>
		{
			try {
				self.queryParams['code'] = params['code'];
				self.queryParams['id'] = params['id'];
			} catch (e) {

			}
		});
	}

	ngOnInit()
	{
	}

	private recover(password, repeat_password)
	{
		let self = this;
		if (password === repeat_password) {
			this.authService.recoveryPasswordConfirm({password: password}, this.queryParams, function (data)
			{
				switch (data['status']) {
					case true:
						window.location.href = '/dashboard';
						break;
					case false:
						self.alert.alert({
							type: 'error',
							title: 'Oops',
							text: 'Wrong credentials, please try again recover you password'
						});
						break;
				}
			});
		} else {
			self.alert.alert({
				type: 'warning',
				title: 'Oops',
				text: 'Password is different, please try again'
			});
		}
	}

}
