export var filters:Object =
{
	sb_min: {
		name: "sb_rank",
		key: "min_sb",
		type: 'select',
		value: [
			{
				name: 0,
				value: 0,
				selected: true
			},
			{
				name: 1,
				value: 1,
				selected: false
			},
			{
				name: 2,
				value: 2,
				selected: false
			},
			{
				name: 3,
				value: 3,
				selected: false
			},
			{
				name: 4,
				value: 4,
				selected: false
			},
			{
				name: 5,
				value: 5,
				selected: false
			},
			{
				name: 6,
				value: 6,
				selected: false
			},
			{
				name: 7,
				value: 7,
				selected: false
			},
			{
				name: 8,
				value: 8,
				selected: false
			},
			{
				name: 9,
				value: 9,
				selected: false
			},
			{
				name: 10,
				value: 10,
				selected: false
			}
		],
		min: 0,
		max: 10,
		range: [
			
		],
		form: 'fb',
		_class: "form-control",
		style: "font-size: 10px;"
	},
	sb_max: {
		name: "sb_rank",
		key: "max_sb",
		type: 'select',
		value: [
			{
				name: 0,
				value: 0,
				selected: false
			},
			{
				name: 1,
				value: 1,
				selected: false
			},
			{
				name: 2,
				value: 2,
				selected: false
			},
			{
				name: 3,
				value: 3,
				selected: false
			},
			{
				name: 4,
				value: 4,
				selected: false
			},
			{
				name: 5,
				value: 5,
				selected: false
			},
			{
				name: 6,
				value: 6,
				selected: false
			},
			{
				name: 7,
				value: 7,
				selected: false
			},
			{
				name: 8,
				value: 8,
				selected: false
			},
			{
				name: 9,
				value: 9,
				selected: false
			},
			{
				name: 10,
				value: 10,
				selected: true
			}
		],
		min: 0,
		max: 10,
		form: 'fb',
		_class: "form-control",
		style: "font-size: 10px;"
	}
}