export var filtersForms: Object =
    {
        fb: {
            name: 'fb'
        },
        balance: {
            name: 'balance'
        },
        addtocart: {
            name: 'addtocart'
        },
        change_in_cart: {
            name: 'change_in_cart'
        },
        change_in_cart_global: {
            name: 'change_in_cart_global'
        },
        cart: {
            name: 'cart'
        }
    }

export var filtersNames = function () {
    return Object.keys(filtersForms);
}