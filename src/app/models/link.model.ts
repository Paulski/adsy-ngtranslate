export interface Link
{
	category_name:string,
	country_iso:string,
	page_id:string,
	page_language:string,
	page_param_ol:string,
	page_param_pr:number,
	page_param_sb:string,
	page_rating:number,
	page_seomoz_rank_autority:string,
	page_url:string,
	page_user_cost:number,
	site_country_id:number,
	site_placement_percent:number,
	site_seomoz_domain_authority:number
}