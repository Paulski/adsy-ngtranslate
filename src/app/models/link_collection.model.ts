import {Link} from './link.model';

declare var _:any;

export class ListLinks<Link>
{
	private links:Array<Link> = [];

	constructor(links:Link[])
	{
		for (var item of links) {
			this.links.push(item);
		}
	}

	public size():number
	{
		if (typeof this.links.length != 'undefined') {
			return this.links.length;
		}
		return 0;
	}

	public add(value:Link):void
	{
		this.links.push(value);
	}

	public get(index:number):Link
	{
		return this.links[index];
	}

	public getAll(form):Array<any>
	{
		return this.links;
	}

	public getLinksById(linkID:number):Link
	{
		return _.find(this.links, function (obj)
		{
			return obj.page_id == linkID;
		});
	}
}