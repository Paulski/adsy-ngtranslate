export interface IKeyword
{
    keyword_id: number
    keyword_text: string,
    keyword_project_id: number,
    keyword_for_plan: number
}