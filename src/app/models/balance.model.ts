export interface IBalance
{
	transfer_id:number;
	transfer_amount:number;
	transfer_user_balance:number;
	transfer_code_id:number;
	transfer_time:number;
	code_name:string;
	source_title:string;
}