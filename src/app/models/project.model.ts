export interface IProject
{
    project_id: number
    project_url: string,
    project_domain: string
}