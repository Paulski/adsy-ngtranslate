// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.
export const env = {
  API_BASE_URL: "http://new.cp.linksmanagement.com.local",
  API_AUTH_URL: "http://new.cp.linksmanagement.com.local/user/security/login",
  API_REGISTER_URL: "http://new.cp.linksmanagement.com.local/user/registration/register",
  API_TRANSLATION_URL: "http://new.cp.linksmanagement.com.local/translations/get-translations?",
  CHROME_BIN: "/usr/bin/chromium-browser",
  production: true,
  envName: 'dev'
};
