import './polyfills.ts';

import { env } from './environments/environment';

// import { AppComponent } from './app/components/app/app.component';
//
// if (environment.production) {
//   enableProdMode();
// }
//
// bootstrap(AppComponent);


import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app/app.module';

platformBrowserDynamic().bootstrapModule(AppModule);